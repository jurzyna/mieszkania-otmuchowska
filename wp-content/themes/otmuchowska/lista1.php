<?php
/*
Template Name: Lista 1
*/
?>
<?php get_header();?>
	
	<section id="content-wrap">
		<div id="main">
			
			<?php the_post(); ?>
			
			<article id="post-<?php the_ID();?>" <?php post_class('entry-wrap');?>>
				<?php if ( has_post_thumbnail()): ?>
				<div class="entry-thumb">
					<?php the_post_thumbnail('fullwidth-page-image'); ?>
				</div>
				<?php endif; ?>
				
				<h1 class="entry-title"><?php the_title(); ?></h1>
				
				<div class="entry-content">
					<div class="entry-content">
										<div class="tabs ui-tabs ui-widget ui-widget-content ui-corner-all"><ul class="tabset ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"><li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tab-0" class="tab">Klatka 54C</a></li><li class="ui-state-default ui-corner-top"><a href="#tab-1" class="tab">Klatka 54D</a></li><li class="clear"></li></ul><div id="tab-0" class="tabContent ui-tabs-panel ui-widget-content ui-corner-bottom" style=""><p><div class="one_fifth_lista content_left listaTitle"><p>NUMER MIESZKANIA</p></div>
										<div class="one_fifth_lista content_left listaTitle"><p>LOKALIZACJA</p></div>
										<div class="one_fifth_lista content_left listaTitle"><p>POWIERZCHNIA</p></div>				
										<div class="one_fifth_lista content_left listaTitle"><p>BALKON</p></div>
										<div class="one_fifth_lista_last content_left listaTitle"><p>STATUS</p></div><div class="<?php if(get_option_tree('1c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('1c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('1c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>1C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>65,9</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('1c')) { ?><p><?php echo get_option_tree('1c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('2c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('2c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('2c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>2C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>112,9</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('2c')) { ?><p><?php echo get_option_tree('2c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('3c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('3c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('3c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>3C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>53,5</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('3c')) { ?><p><?php echo get_option_tree('3c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('4c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('4c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('4c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>4C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>76</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('4c')) { ?><p><?php echo get_option_tree('4c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('5c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('5c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('5c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>5C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>87,3</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('5c')) { ?><p><?php echo get_option_tree('5c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('6c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('6c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('6c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>6C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>66,3</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('6c')) { ?><p><?php echo get_option_tree('6c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('7c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('7c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('7c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>7C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>113,37</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('7c')) { ?><p><?php echo get_option_tree('7c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('8c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('8c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('8c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>8C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>54,9</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('8c')) { ?><p><?php echo get_option_tree('8c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('9c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('9c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('9c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>9C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>77,5</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('9c')) { ?><p><?php echo get_option_tree('9c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('10c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('10c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('10c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>10C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>89,5</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('10c')) { ?><p><?php echo get_option_tree('10c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('11c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('11c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('11c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>11C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>67,8</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('11c')) { ?><p><?php echo get_option_tree('11c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('12c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('12c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('12c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>12C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>112,96</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('12c')) { ?><p><?php echo get_option_tree('12c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('13c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('13c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('13c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>13C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>52,8</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('13c')) { ?><p><?php echo get_option_tree('13c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('14c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('14c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('14c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>14C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>81,1</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('14c')) { ?><p><?php echo get_option_tree('14c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('15c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('15c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('15c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>15C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>90</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('15c')) { ?><p><?php echo get_option_tree('15c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('16c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('16c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('16c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>16C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>59,1</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('16c')) { ?><p><?php echo get_option_tree('16c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('17c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('17c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('17c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>17C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>71,6</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('17c')) { ?><p><?php echo get_option_tree('17c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('18c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('18c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('18c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>18C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>73,05</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('18c')) { ?><p><?php echo get_option_tree('18c'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('19c', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('19c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('19c', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>19C</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>93</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('19c')) { ?><p><?php echo get_option_tree('19c'); ?></p><?php } ?></div>
										</div>
										</p>
					</div><div id="tab-1" class="tabContent ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" style=""><p><div class="one_fifth_lista content_left listaTitle"><p>NUMER MIESZKANIA</p></div>
						<div class="one_fifth_lista content_left listaTitle"><p>LOKALIZACJA</p></div>
						<div class="one_fifth_lista content_left listaTitle"><p>POWIERZCHNIA</p></div>				
						<div class="one_fifth_lista content_left listaTitle"><p>BALKON</p></div>
						<div class="one_fifth_lista_last content_left listaTitle"><p>STATUS</p></div>
						
					<div class="<?php if(get_option_tree('1d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('1d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('1d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>1D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>50,8</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('1d')) { ?><p><?php echo get_option_tree('1d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('2d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('2d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('2d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>2D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>49,5</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('2d')) { ?><p><?php echo get_option_tree('2d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('3d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('3d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('3d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>3D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>51,4</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('3d')) { ?><p><?php echo get_option_tree('3d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('4d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('4d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('4d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>4D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>54, 2</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('4d')) { ?><p><?php echo get_option_tree('4d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('5d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('5d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('5d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>5D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>53</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>brak</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('5d')) { ?><p><?php echo get_option_tree('5d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('6d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('6d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('6d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>6D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>78,3</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('6d')) { ?><p><?php echo get_option_tree('6d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('7d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('7d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('7d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>7D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>52,2</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('7d')) { ?><p><?php echo get_option_tree('7d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('8d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('8d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('8d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>8D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>51,8</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>brak</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('8d')) { ?><p><?php echo get_option_tree('8d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('9d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('9d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('9d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>9D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>53,7</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('9d')) { ?><p><?php echo get_option_tree('9d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('10d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('10d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('10d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>10D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>54,4</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('10d')) { ?><p><?php echo get_option_tree('10d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('11d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('11d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('11d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>11D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>53,9</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>brak</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('4d')) { ?><p><?php echo get_option_tree('11d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('12d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('12d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('12d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>12D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>79,8</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('12d')) { ?><p><?php echo get_option_tree('12d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('13d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('13d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('13d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>13D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>52,2</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('13d')) { ?><p><?php echo get_option_tree('13d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('14d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('14d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('14d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>14D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>51,4</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>brak</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('14d')) { ?><p><?php echo get_option_tree('14d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('15d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('15d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('15d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>15D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>54,1</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('15d')) { ?><p><?php echo get_option_tree('15d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('16d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('16d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('16d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>16D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>54,3</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('16d')) { ?><p><?php echo get_option_tree('16d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('17d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('17d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('17d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>17D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>71,6</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('17d')) { ?><p><?php echo get_option_tree('17d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('18d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('18d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('18d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>18D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>79,3</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('18d')) { ?><p><?php echo get_option_tree('18d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('19d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('19d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('19d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>19D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>67</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('19d')) { ?><p><?php echo get_option_tree('19d'); ?></p><?php } ?></div>
					</div>
					
					
					<div class="<?php if(get_option_tree('20d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('20d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('20d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>20D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>87,6</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('20d')) { ?><p><?php echo get_option_tree('20d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('21d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('21d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('21d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>21D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>82,53</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>2</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('21d')) { ?><p><?php echo get_option_tree('21d'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('22d', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('22d', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('22d', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>22D</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>61,6</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('22d')) { ?><p><?php echo get_option_tree('22d'); ?></p><?php } ?></div>
					</div>
					
					
							</p>
					</div></div>					
															
					
									</div>
					
					<?php wp_link_pages( array( 'before' => '<p><span>' . __( 'Pages:', 'premitheme' ) . '</span>', 'after' => '</p>' ) ); ?>
					

				</div>
			</article>
			
		</div><!-- #main -->
		
<?php get_footer();?>
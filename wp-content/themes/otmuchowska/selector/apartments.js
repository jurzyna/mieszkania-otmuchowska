$(document).ready(function() {
        	  
        	/***************************************
        			Mouse Hover Events
        	*****************************************/
			$(".box_selection").hover(
			  function () {
			  	var selection = $(this).attr('class').split(" ")[2];
			  	var divBox = '#' + selection;
			  	var divNav = '.sidebar_' + selection;
			  		
			  		if($(this).hasClass('hover')){
			  			//alert(divBox);
			  		   $(divBox).stop().fadeTo(200, 1);
			  		   if($(divNav).hasClass('available')){
				  		   $(divNav).animate({
				  		         'background-color': '#007167',
				  		         'color': '#fff'
				  		     }, 200 );
			  		     }
			  		     if($(divNav).hasClass('sold')){
			  		     	   $(divNav).animate({
			  		     	         'background-color': '#8a2e2e',
			  		     	         'color': '#fff'
			  		     	     }, 200 );
			  		       }
			  		  if($(divNav).hasClass('reserved')){
			  		  	   $(divNav).animate({
			  		  	         'background-color': '#ab9d00',
			  		  	         'color': '#fff'
			  		  	     }, 200 );
			  		    }
			  		}
			  		
			  },
			  function () {
				var selection = $(this).attr('class').split(" ")[2];
				var divBox = '#' + selection;
				var divNav = '.sidebar_' + selection;
				
					
					if($(this).hasClass('hover')){
					   $(divBox).stop().fadeTo(200, 0);
					   $(divNav).animate({
					         'background-color': '#fff',
					         'color': '#000'
					     }, 200 );
					}
			  }
			);  
        	
        	  
        	        	
        	/***************************************
    			Mouse Click Events
    	*****************************************/
        	
        	
        	$('.box_selection').click(function() {
        	    	  var selection = $(this).attr('class').split(" ")[2];
        	    	  var divBox = '#' + selection;
        	    	  var divNav = '.sidebar_' + selection;
        	    	  var divMap = 'area.' + selection;
        	    	  var infoSide = '.info_' + selection;
        	    	  var loadInfo = 'infos.html ' + infoSide;
        	    	  
        	    	  //if($(this).hasClass('hover')){
        	    	      	  	  $('.box_selection').not(divMap  + "," + divNav).removeClass("clicked").addClass("hover");
        	    	      	  	  
        	    	      	  	  $('a.box_selection').not(divNav).animate({
        	    	      	  	          'background-color': '#fff',
        	    	      	  	          'color': '#000'
        	    	      	  	      }, 200);
        	    	      	  	  
        	    	      	  	  $('ul.group_list li').removeClass('selected').stop().fadeTo(200, 0);
        	    	      	  	  
        	    	      	  	  $(divMap  + "," + divNav).addClass('clicked').toggleClass('hover');
        	    	      	  	  
        	    	      	  	  $(divBox).toggleClass('selected').stop().fadeTo(200, 1);
        	    	      	  	
									if($(divNav).hasClass('available')){
	        	    	      	  	  $(divNav).animate({
	        	    	      	  	              'background-color': '#007167',
	        	    	      	  	              'color': '#fff'
	        	    	      	  	          }, 200);
        	    	  				}
        	    	      	    //}
        	    	      	    
        	    	      	    
        	    	      	    if($(infoSide).hasClass('display')){
        	    	      	    	$('.info_box').slideUp('fast');
        	    	      	    	//$(infoSide).slideUp('fast');
        	    	      	    	$(infoSide).removeClass("display");
        	    	      	    
        	    	      	    }
        	    	      	    else {
        	    	      	    	showLoading();
        	    	      	    	$('.info_box').slideUp('fast');
        	    	      	    	$(infoSide).load(loadInfo, hideLoading).slideDown('fast');
        	    	      	    	$(infoSide).addClass("display");
        	    	      	    }
        	    	      	
						function showLoading(){
							$(divNav).siblings('.loader').slideDown("fast").css({visibility:"visible"}).css({opacity:"1"});
						}
						
						function hideLoading(){
							$(divNav).siblings('.loader').fadeTo(1000, 0).slideUp("fast");
						}
        	    	    
        	    	  });
        	
        	
        		
        	
        	$(".box_selection, .box_selection_level").click(function(){
        	 	var nameId = $(this).attr('class').split(" ")[1];
        	    var newClass = "#l" + nameId;
        	    var useMap = '#map' + (nameId);
        	    var levelTitle = '.level_title_' + (nameId);
        	    var levelBg = "#l" + nameId + "_front_bg";
        	    
        	   	if($(newClass).prev().hasClass('down')){
        	    	$(newClass).prevAll(".down").animate({"top": "-=120px"}, "slow").toggleClass("up").removeClass("down");
        	    }
        	        
        	   	else if($(newClass).hasClass('up')){
        	    	$(newClass).animate({"top": "+=120px"}, "slow").toggleClass("down").removeClass("up");
        	    	$(newClass).nextAll(".up").animate({"top": "+=120px"}, "slow").toggleClass("down").removeClass("up");
        	    					         	
        	       }
        	       
        	    if($(levelTitle).hasClass('down')){
        	    	$('.group_list_side').slideUp('fast');
        	    	$('.group_list_side').siblings("a").addClass("down").removeClass("up");
        	    	$(levelTitle).siblings("ul").slideDown('fast');
        	    	$(levelTitle).toggleClass("up").removeClass("down");
        	    }
        	    
        	    if ( $('#l7').hasClass("up") && $('#l6').hasClass("down") ) {
        	    	$('#l7_front_bg').addClass('itselfup');
        	    } else {
        	    	$('#l7_front_bg').removeClass('itselfup');
        	    }
        	    /*
        	    //alert(levelBg);
        	    $('.front_bg').removeClass("current")
        	    $(levelBg).addClass("current"); */
        	    $("#transparent_full").attr('usemap', (useMap));
        	   
        	   	}); 
        	   	
        	   	
        		
				
        	})
<!--  Map 3  -->
<map name="map3" id="map3">

	<area shape="poly" alt="" coords="85,254, 97,261, 42,286, 42,304, 0,279, 0,262, 47,240, 47,240, 77,258, 85,254, 85,254" href="#" class="box_selection 3 l3_17B hover"/>
	<area shape="poly" alt="" coords="124,236, 124,253, 119,251, 97,261, 85,254, 85,254, 77,258, 47,240, 94,219, 124,236" href="#" class="box_selection 3 l3_18B hover"/>
	<area shape="poly" alt="" coords="151,269, 151,287, 73,322, 42,304, 42,286, 97,261, 119,251, 124,253, 124,254, 136,260, 151,269, 151,269" href="#" class="box_selection 3 l3_16B hover"/>
	<area shape="poly" alt="" coords="180,223, 154,235, 143,240, 136,243, 136,248, 124,254, 124,253, 124,236, 94,219, 139,198, 180,223" href="#" class="box_selection 3 l3_19B hover"/>
	<area shape="poly" alt="" coords="212,241, 212,259, 167,279, 151,269, 151,269, 136,260, 136,248, 136,243, 143,240, 154,235, 180,223, 180,223, 212,241" href="#" class="box_selection 3 l3_20B hover"/>
	<area shape="poly" alt="" coords="284,209, 284,226, 212,259, 212,241, 180,223, 233,199, 252,190, 257,193, 257,193, 269,200, 284,208, 284,209" href="#" class="box_selection 3 l3_19A hover"/>
	<area shape="poly" alt="" coords="233,199, 180,223, 139,198, 167,185, 197,203, 221,192, 233,199" href="#" class="box_selection 3 l3_20A hover"/>
	<area shape="poly" alt="" coords="257,175, 257,193, 252,190, 233,199, 221,192, 221,192, 197,203, 167,185, 227,158, 257,175" href="#" class="box_selection 3 l3_21A hover"/>
	<area shape="poly" alt="" coords="348,146, 288,173, 276,179, 276,166, 289,161, 259,143, 306,122, 348,146" href="#" class="box_selection 3 l3_23A hover"/>
	<area shape="poly" alt="" coords="379,165, 379,182, 300,218, 284,209, 284,208, 269,200, 269,188, 269,182, 276,179, 288,173, 348,146, 379,165" href="#" class="box_selection 3 l3_24A hover"/>
	<area shape="poly" alt="" coords="289,161, 276,166, 276,179, 269,182, 269,188, 257,193, 257,193, 257,175, 227,158, 259,143, 289,161" href="#" class="box_selection 3 l3_22A hover"/>
	<area shape="poly" alt="" coords="0,279, 0,297, 42,321, 42,304, 0,279" href="#" class="box_selection 2 l2_12B hover"/>
	<area shape="poly" alt="" coords="94,312, 73,322, 42,304, 42,321, 73,340, 94,330, 101,334, 112,328, 112,322, 123,317, 130,320, 142,315, 142,308, 151,304, 151,287, 151,287, 94,312" href="#" class="box_selection 2 l2_11B hover"/>
	<area shape="poly" alt="" coords="195,266, 167,279, 151,269, 151,287, 151,287, 167,297, 183,290, 189,293, 201,288, 201,281, 212,276, 212,259, 195,266" href="#" class="box_selection 2 l2_15B hover"/>
	<area shape="poly" alt="" coords="212,259, 212,276, 218,280, 230,274, 230,268, 254,257, 254,239, 212,259" href="#" class="box_selection 2 l2_14A hover"/>
	<area shape="poly" alt="" coords="258,238, 254,239, 254,257, 258,255, 264,259, 275,254, 275,247, 284,243, 284,226, 284,226, 258,238" href="#" class="box_selection 2 l2_13A hover"/>
	<area shape="poly" alt="" coords="317,210, 300,218, 284,209, 284,226, 284,226, 300,236, 317,228, 323,232, 335,226, 335,220, 379,200, 379,182, 317,210" href="#" class="box_selection 2 l2_18A hover"/>
	<area shape="poly" alt="" coords="142,309, 142,315, 130,320, 124,317, 112,322, 112,328, 100,334, 94,330, 73,340, 42,321, 42,339, 73,357, 94,348, 101,351, 112,346, 112,340, 123,334, 130,338, 142,332, 142,326, 151,322, 151,305, 151,304, 142,309" href="#" class="box_selection 1 l1_6B hover"/>
	<area shape="poly" alt="" coords="201,281, 201,288, 189,293, 183,290, 167,297, 151,287, 151,304, 151,305, 167,314, 183,307, 189,311, 201,305, 201,299, 212,294, 212,276, 201,281" href="#" class="box_selection 1 l1_10B hover"/>
	<area shape="poly" alt="" coords="230,268, 230,275, 218,280, 212,276, 212,294, 218,298, 230,292, 230,286, 254,275, 254,257, 230,268" href="#" class="box_selection 1 l1_8A hover"/>
	<area shape="poly" alt="" coords="275,247, 275,254, 263,259, 258,256, 254,257, 254,275, 258,273, 264,277, 275,271, 275,265, 284,261, 284,244, 284,244, 275,247" href="#" class="box_selection 1 l1_7A hover"/>
	<area shape="poly" alt="" coords="335,220, 335,226, 323,232, 317,228, 300,236, 284,226, 284,243, 284,244, 300,254, 317,246, 323,249, 335,244, 335,238, 379,218, 379,200, 335,220" href="#" class="box_selection 1 l1_12A hover"/>
	<area shape="poly" alt="" coords="42,321, 42,339, 0,314, 0,297, 42,321" href="#" class="box_selection 1 l1_7B hover"/>
	<area shape="poly" alt="" coords="142,326, 142,333, 130,338, 124,335, 112,340, 112,346, 100,351, 94,348, 73,357, 42,339, 42,357, 73,375, 94,365, 101,369, 112,364, 112,357, 123,352, 130,356, 142,350, 142,344, 151,340, 151,322, 151,322, 142,326" href="#" class="box_selection 0 l0_1B hover"/>
	<area shape="poly" alt="" coords="201,299, 201,305, 189,311, 183,307, 167,314, 151,305, 151,322, 151,322, 167,332, 183,325, 189,328, 201,323, 201,317, 212,312, 212,294, 201,299" href="#" class="box_selection 0 l0_5B hover"/>
	<area shape="poly" alt="" coords="230,292, 218,298, 212,294, 212,312, 218,315, 230,310, 230,303, 254,292, 254,275, 230,286, 230,292" href="#" class="box_selection 0 l0_2A hover"/>
	<area shape="poly" alt="" coords="275,265, 275,271, 263,277, 258,273, 254,275, 254,292, 258,291, 264,294, 275,289, 275,282, 284,279, 284,261, 284,261, 275,265" href="#" class="box_selection 0 l0_1A hover"/>
	<area shape="poly" alt="" coords="335,238, 335,244, 323,249, 317,246, 300,254, 284,244, 284,261, 284,261, 300,271, 317,263, 323,267, 335,262, 335,255, 379,235, 379,218, 335,238" href="#" class="box_selection 0 l0_6A hover"/>
	<area shape="poly" alt="" coords="42,339, 42,357, 0,332, 0,314, 42,339" href="#" class="box_selection 0 l0_2B hover"/>
							
</map>
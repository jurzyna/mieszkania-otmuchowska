<!--  Map 0  -->
<map name="map0" id="map0">
	<area shape="poly" alt="" coords="142,189, 142,195, 130,200, 124,197, 112,202, 112,208, 100,214, 94,210, 73,220, 42,201, 42,219, 73,237, 94,228, 101,231, 112,226, 112,220, 123,214, 130,218, 142,212, 142,206, 151,202, 151,185, 151,184, 142,189" href="#" class="box_selection 1 l1_6B">
	<area shape="poly" alt="" coords="201,161, 201,168, 189,173, 183,170, 167,177, 151,167, 151,184, 151,185, 167,194, 183,187, 189,191, 201,185, 201,179, 212,174, 212,156, 201,161" href="#" class="box_selection 1 l1_10B">
	<area shape="poly" alt="" coords="230,148, 230,155, 218,160, 212,156, 212,174, 218,178, 230,172, 230,166, 254,155, 254,137, 230,148" href="#" class="box_selection 1 l1_8A">
	<area shape="poly" alt="" coords="275,127, 275,134, 263,139, 258,136, 254,137, 254,155, 258,153, 264,157, 275,151, 275,145, 284,141, 284,124, 284,124, 275,127" href="#" class="box_selection 1 l1_7A">
	<area shape="poly" alt="" coords="335,100, 335,106, 323,112, 317,108, 300,116, 284,106, 284,123, 284,124, 300,134, 317,126, 323,129, 335,124, 335,118, 379,98, 379,80, 335,100" href="#" class="box_selection 1 l1_12A">
	<area shape="poly" alt="" coords="42,201, 42,219, 0,194, 0,177, 42,201" href="#" class="box_selection 1 l1_7B">
	<area shape="poly" alt="" coords="0,159, 0,177, 42,202, 42,184, 0,159" href="#" class="box_selection 2 l2_12B">
	<area shape="poly" alt="" coords="94,193, 73,202, 42,184, 42,202, 73,220, 94,210, 101,214, 112,209, 112,202, 123,197, 130,201, 142,195, 142,189, 151,184, 151,167, 151,167, 94,193" href="#" class="box_selection 2 l2_11B">
	<area shape="poly" alt="" coords="195,147, 167,159, 151,150, 151,167, 151,167, 167,177, 183,170, 189,173, 201,168, 201,162, 212,157, 212,139, 195,147" href="#" class="box_selection 2 l2_15B">
	<area shape="poly" alt="" coords="212,139, 212,157, 218,160, 230,155, 230,148, 254,137, 254,120, 212,139" href="#" class="box_selection 2 l2_14A">
	<area shape="poly" alt="" coords="258,118, 254,120, 254,137, 258,136, 264,139, 275,134, 275,127, 284,124, 284,106, 284,106, 258,118" href="#" class="box_selection 2 l2_13A">
	<area shape="poly" alt="" coords="317,91, 300,98, 284,89, 284,106, 284,106, 300,116, 317,108, 323,112, 335,107, 335,100, 379,80, 379,62, 317,91" href="#" class="box_selection 2 l2_18A">
	<area shape="poly" alt="" coords="85,134, 97,141, 42,166, 42,184, 0,159, 0,142, 47,120, 47,120, 77,138, 85,134, 85,134" href="#" class="box_selection 3 l3_17B">
	<area shape="poly" alt="" coords="124,116, 124,133, 119,131, 97,141, 85,134, 85,134, 77,138, 47,120, 94,99, 124,116" href="#" class="box_selection 3 l3_18B">
	<area shape="poly" alt="" coords="151,149, 151,167, 73,202, 42,184, 42,166, 97,141, 119,131, 124,133, 124,134, 136,140, 151,149, 151,149" href="#" class="box_selection 3 l3_16B">
	<area shape="poly" alt="" coords="180,103, 154,115, 143,120, 136,123, 136,128, 124,134, 124,133, 124,116, 94,99, 139,78, 180,103" href="#" class="box_selection 3 l3_19B">
	<area shape="poly" alt="" coords="212,121, 212,139, 167,159, 151,149, 151,149, 136,140, 136,128, 136,123, 143,120, 154,115, 180,103, 180,103, 212,121" href="#" class="box_selection 3 l3_20B">
	<area shape="poly" alt="" coords="284,89, 284,106, 212,139, 212,121, 180,103, 233,79, 252,70, 257,73, 257,73, 269,80, 284,88, 284,89" href="#" class="box_selection 3 l3_19A">
	<area shape="poly" alt="" coords="233,79, 180,103, 139,78, 167,65, 197,83, 221,72, 233,79" href="#" class="box_selection 3 l3_20A">
	<area shape="poly" alt="" coords="257,55, 257,73, 252,70, 233,79, 221,72, 221,72, 197,83, 167,65, 227,38, 257,55" href="#" class="box_selection 3 l3_21A">
	<area shape="poly" alt="" coords="348,26, 288,53, 276,59, 276,46, 289,41, 259,23, 306,2, 348,26" href="#" class="box_selection 3 l3_23A">
	<area shape="poly" alt="" coords="379,45, 379,62, 300,98, 284,89, 284,88, 269,80, 269,68, 269,62, 276,59, 288,53, 348,26, 379,45" href="#" class="box_selection 3 l3_24A">
	<area shape="poly" alt="" coords="289,41, 276,46, 276,59, 269,62, 269,68, 257,73, 257,73, 257,55, 227,38, 259,23, 289,41" href="#" class="box_selection 3 l3_22A">
	<area shape="poly" alt="" coords="97,314, 42,339, 42,357, 0,332, 0,314, 15,308, 15,301, 26,296, 33,300, 44,294, 47,293, 77,311, 85,307, 97,314" href="#" class="box_selection 0 l0_2B">
	<area shape="poly" alt="" coords="124,289, 124,306, 119,304, 97,314, 85,307, 77,311, 47,293, 44,294, 44,288, 56,282, 62,286, 94,271, 124,289" href="#" class="box_selection 0 l0_3B">
	<area shape="poly" alt="" coords="124,306, 136,313, 151,322, 151,322, 151,340, 142,344, 142,350, 129,356, 123,352, 112,357, 112,364, 100,369, 94,365, 73,375, 42,357, 42,339, 97,314, 119,304, 124,306, 124,306" href="#" class="box_selection 0 l0_1B">
	<area shape="poly" alt="" coords="180,276, 154,288, 143,293, 136,296, 136,301, 124,306, 124,306, 124,289, 94,271, 103,267, 103,261, 115,255, 121,259, 139,251, 180,276" href="#" class="box_selection 0 l0_4B">
	<area shape="poly" alt="" coords="212,294, 212,312, 201,317, 201,323, 189,328, 183,325, 167,332, 151,322, 151,322, 136,313, 136,301, 136,296, 143,293, 154,288, 180,276, 212,294" href="#" class="box_selection 0 l0_5B">
	<!--<area shape="poly" alt="" coords="254,275, 254,292, 230,303, 230,310, 218,315, 212,312, 212,294, 180,276, 139,251, 141,250, 141,243, 153,238, 159,242, 167,238, 197,256, 221,245, 233,251, 222,256, 254,275" href="#" class="box_selection 0 l0_2A">-->
	<area shape="poly" alt="" coords="222,263, 193,276, 181,269, 168,276, 180,283, 180,283, 211,301, 211,319, 217,322, 229,317, 229,310, 253,299, 253,282, 222,263" href="#" class="box_selection 0 l0_2A">
	<area shape="poly" alt="" coords="203,259, 196,263, 166,245, 158,249, 152,245, 140,250, 140,257, 138,258, 168,276, 181,269, 193,276, 215,266, 203,259" href="#" class="box_selection 0 l0_2Av2">3

	<area shape="poly" alt="" coords="259,227, 259,245, 257,246, 252,243, 233,251, 221,245, 197,256, 167,238, 178,233, 178,226, 190,221, 196,225, 229,210, 259,227" href="#" class="box_selection 0 l0_3A">
	<area shape="poly" alt="" coords="284,261, 284,279, 275,282, 275,289, 264,294, 258,291, 254,292, 254,275, 222,256, 233,251, 252,243, 257,246, 269,252, 284,261, 284,261" href="#" class="box_selection 0 l0_1A">
	<area shape="poly" alt="" coords="289,214, 276,219, 276,232, 269,235, 269,240, 259,245, 259,227, 229,210, 237,206, 237,199, 249,194, 256,198, 259,196, 289,214" href="#" class="box_selection 0 l0_4A">
	<area shape="poly" alt="" coords="379,218, 379,235, 335,255, 335,262, 323,267, 317,263, 300,271, 284,261, 284,261, 269,252, 269,240, 269,235, 276,232, 288,226, 348,199, 379,218" href="#" class="box_selection 0 l0_6A">
	<area shape="poly" alt="" coords="348,199, 288,226, 276,232, 276,219, 289,214, 259,196, 267,192, 267,186, 279,180, 286,184, 306,175, 348,199" href="#" class="box_selection 0 l0_5A">
</map>
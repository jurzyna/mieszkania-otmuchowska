
<ul id="accordion">
	<li class="klatka_title">
		Klatka 54A
	</li>
		
		
		
	<li><a href="#" class="box_selection_level 3 level_title_3 level_title down">Poddasze</a>
		
		<ul class="group_list_side" style="display: none;">
			
			
			<li><a href="#" class="box_selection 3 l3_19A sidebar_l3_19A hover <?php if(get_option_tree('19a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('19a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('19a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 22A</a>
				
				<span class="info_box info_l3_19A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 3 l3_20A sidebar_l3_20A hover <?php if(get_option_tree('20a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('20a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('20a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 23A</a>
				
				<span class="info_box info_l3_20A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 3 l3_21A sidebar_l3_21A hover <?php if(get_option_tree('21a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('21a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('21a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 24A</a>
				
				<span class="info_box info_l3_21A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 3 l3_22A sidebar_l3_22A hover <?php if(get_option_tree('22a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('22a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('22a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 25A</a>
				
				<span class="info_box info_l3_22A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 3 l3_23A sidebar_l3_23A hover <?php if(get_option_tree('23a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('23a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('23a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 26A</a>
				
				<span class="info_box info_l3_23A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 3 l3_24A sidebar_l3_24A hover <?php if(get_option_tree('24a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('24a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('24a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 27A</a>
				
				<span class="info_box info_l3_24A hide" style="display: none;" > 
						
				</span>
			</li>

			</ul>
	</li>
	
	<li><a href="#" class="box_selection_level 2 level_title_2 level_title down">II Piętro</a>
		
		<ul class="group_list_side" style="display: none;">
			
			<li><a href="#" class="box_selection 2 l2_13A sidebar_l2_13A hover <?php if(get_option_tree('13a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('13a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('13a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 15A</a>
				
				<span class="info_box info_l2_13A hide" style="display: none;" > 
						
				</span>
			</li>
		
			<li><a href="#" class="box_selection 2 l2_14A sidebar_l2_14A hover <?php if(get_option_tree('14a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('14a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('14a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 16A</a>
				
				<span class="info_box info_l2_14A hide" style="display: none;" > 
						
				</span>
			</li>
			<li><a href="#" class="box_selection 2 l2_14Av2 sidebar_l2_14Av2 hover <?php if(get_option_tree('14av2', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('14av2', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('14av2', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 17A</a>
				
				<span class="info_box info_l2_14Av2 hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 2 l2_15A sidebar_l2_15A hover <?php if(get_option_tree('15a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('15a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('15a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 18A</a>
				
				<span class="info_box info_l2_15A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 2 l2_16A sidebar_l2_16A hover <?php if(get_option_tree('16a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('16a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('16a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 19A</a>
				
				<span class="info_box info_l2_16A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 2 l2_17A sidebar_l2_17A hover <?php if(get_option_tree('17a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('17a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('17a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 20A</a>
				
				<span class="info_box info_l2_17A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 2 l2_18A sidebar_l2_18A hover <?php if(get_option_tree('18a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('18a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('18a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 21A</a>
				
				<span class="info_box info_l2_18A hide" style="display: none;" > 
						
				</span>
			</li>
		
		</ul>
	</li>
	
	<li><a href="#" class="box_selection_level 1 level_title_1 level_title down">I Piętro</a>
		
		<ul class="group_list_side" style="display: none;">
			<li><a href="#" class="box_selection 1 l1_7A sidebar_l1_7A hover <?php if(get_option_tree('7a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('7a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('7a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 8A</a>
				
				<span class="info_box info_l1_7A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 1 l1_8A sidebar_l1_8A hover <?php if(get_option_tree('8a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('8a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('8a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 9A</a>
				
				<span class="info_box info_l1_8A hide" style="display: none;" > 
						
				</span>
			</li>

			<li><a href="#" class="box_selection 1 l1_8Av2 sidebar_l1_8Av2 hover <?php if(get_option_tree('8av2', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('8av2', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('8av2', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 10A</a>
				
				<span class="info_box info_l1_8A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 1 l1_9A sidebar_l1_9A hover <?php if(get_option_tree('9a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('9a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('9a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 11A</a>
				
				<span class="info_box info_l1_9A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 1 l1_10A sidebar_l1_10A hover <?php if(get_option_tree('10a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('10a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('10a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 12A</a>
				
				<span class="info_box info_l1_10A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 1 l1_11A sidebar_l1_11A hover <?php if(get_option_tree('11a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('11a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('11a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 13A</a>
				
				<span class="info_box info_l1_11A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 1 l1_12A sidebar_l1_12A hover <?php if(get_option_tree('12a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('12a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('12a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 14A</a>
				
				<span class="info_box info_l1_12A hide" style="display: none;" > 
						
				</span>
			</li>
		
		
		</ul>
	</li>
	
	<li><a href="#" class="box_selection_level 0 level_title_0 level_title down">Parter</a>
		
		<ul class="group_list_side" style="display: none;">
		
			<li><a href="#" class="box_selection 0 l0_1A sidebar_l0_1A hover <?php if(get_option_tree('1a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('1a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('1a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 1A</a>
				
				<span class="info_box info_l0_1A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 0 l0_2A sidebar_l0_2A hover <?php if(get_option_tree('2a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('2a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('2a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 2A</a>
				
				<span class="info_box info_l0_2A hide" style="display: none;" > 
						
				</span>
			</li>
			<li><a href="#" class="box_selection 0 l0_2Av2 sidebar_l0_2Av2 hover <?php if(get_option_tree('2av2', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('2av2', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('2av2', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 3A</a>
				
				<span class="info_box info_l0_2A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 0 l0_3A sidebar_l0_3A hover <?php if(get_option_tree('3a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('3a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('3a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 4A</a>
				
				<span class="info_box info_l0_3A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 0 l0_4A sidebar_l0_4A hover <?php if(get_option_tree('4a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('4a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('4a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 5A</a>
				
				<span class="info_box info_l0_4A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 0 l0_5A sidebar_l0_5A hover <?php if(get_option_tree('5a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('5a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('5a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 6A</a>
				
				<span class="info_box info_l0_5A hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 0 l0_6A sidebar_l0_6A hover <?php if(get_option_tree('6a', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('6a', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('6a', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 7A</a>
				
				<span class="info_box info_l0_6A hide" style="display: none;" > 
						
				</span>
			</li>
		
		</ul>
	</li>
	
</ul>
										
<ul id="accordion2">
	<li class="klatka_title">
		Klatka 54B
	</li>
		
		<li><a href="#" class="box_selection_level 3 level_title_3 level_title down">Poddasze</a>
			
			<ul class="group_list_side" style="display: none;">
				
				<li><a href="#" class="box_selection 3 l3_16B sidebar_l3_16B hover <?php if(get_option_tree('16b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('16b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('16b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 16B</a>
					
					<span class="info_box info_l3_16B hide" style="display: none;" > 
							
					</span>
				</li>									  						
				
				<li><a href="#" class="box_selection 3 l3_17B sidebar_l3_17B hover <?php if(get_option_tree('17b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('17b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('17b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 17B</a>
					
					<span class="info_box info_l3_17B hide" style="display: none;" > 
							
					</span>
				</li>
				
				<li><a href="#" class="box_selection 3 l3_18B sidebar_l3_18B hover <?php if(get_option_tree('18b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('18b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('18b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 18B</a>
					
					<span class="info_box info_l3_18B hide" style="display: none;" > 
							
					</span>
				</li>
				
				<li><a href="#" class="box_selection 3 l3_19B sidebar_l3_19B hover <?php if(get_option_tree('19b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('19b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('19b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 19B</a>
					
					<span class="info_box info_l3_19B hide" style="display: none;" > 
							
					</span>
				</li>
				
				<li><a href="#" class="box_selection 3 l3_20B sidebar_l3_20B hover <?php if(get_option_tree('20b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('20b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('20b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 20B</a>
					
					<span class="info_box info_l3_20B hide" style="display: none;" > 
							
					</span>
				</li>
				
			</ul>
		</li>
	
	
	<li><a href="#" class="box_selection_level 2 level_title_2 level_title down">II Piętro</a>
		
		<ul class="group_list_side" style="display: none;">
		
			<li><a href="#" class="box_selection 2 l2_11B sidebar_l2_11B hover <?php if(get_option_tree('11b', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('11b', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('11b', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 11B</a>
				
				<span class="info_box info_l2_11B hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 2 l2_12B sidebar_l2_12B hover <?php if(get_option_tree('12b', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('12b', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('12b', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 12B</a>
				
				<span class="info_box info_l2_12B hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 2 l2_13B sidebar_l2_13B hover <?php if(get_option_tree('13b', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('13b', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('13b', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 13B</a>
				
				<span class="info_box info_l2_13B hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 2 l2_14B sidebar_l2_14B hover <?php if(get_option_tree('14b', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('14b', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('14b', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 14B</a>
				
				<span class="info_box info_l2_14B hide" style="display: none;" > 
						
				</span>
			</li>
			
			<li><a href="#" class="box_selection 2 l2_15B sidebar_l2_15B hover <?php if(get_option_tree('15b', '', false, true, 0 ) == "Wolne" ) {
			echo "available"; } ?><?php if(get_option_tree('15b', '', false, true, 0 ) == "Sprzedane" ) {
			echo "sold"; } ?><?php if(get_option_tree('15b', '', false, true, 0 ) == "Rezerwacja" ) {
			echo "reserved"; } ?>">Mieszkanie 15B</a>
				
				<span class="info_box info_l2_15B hide" style="display: none;" > 
						
				</span>
			</li>
			
		</ul>
	</li>						  				
		
		<li><a href="#" class="box_selection_level 1 level_title_1 level_title down">I Piętro</a>
			
			<ul class="group_list_side" style="display: none;">

				
				<li><a href="#" class="box_selection 1 l1_6B sidebar_l1_6B hover <?php if(get_option_tree('6b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('6b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('6b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 6B</a>
					
					<span class="info_box info_l1_6B hide" style="display: none;" > 
							
					</span>
				</li>
				
				<li><a href="#" class="box_selection 1 l1_7B sidebar_l1_7B hover <?php if(get_option_tree('7b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('7b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('7b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 7B</a>
					
					<span class="info_box info_l1_7B hide" style="display: none;" > 
							
					</span>
				</li>
				
				<li><a href="#" class="box_selection 1 l1_8B sidebar_l1_8B hover <?php if(get_option_tree('8b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('8b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('8b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 8B</a>
					
					<span class="info_box info_l1_8B hide" style="display: none;" > 
							
					</span>
				</li>
				
				<li><a href="#" class="box_selection 1 l1_9B sidebar_l1_9B hover <?php if(get_option_tree('9b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('9b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('9b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 9B</a>
					
					<span class="info_box info_l1_9B hide" style="display: none;" > 
							
					</span>
				</li>
				
				<li><a href="#" class="box_selection 1 l1_10B sidebar_l1_10B hover <?php if(get_option_tree('10b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('10b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('10b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 10B</a>
					
					<span class="info_box info_l1_10B hide" style="display: none;" > 
							
					</span>
				</li>
			</ul>
		</li>
		
		<li><a href="#" class="box_selection_level 0 level_title_0 level_title down">Parter</a>
			
			<ul class="group_list_side" style="display: none;">
				
				<li><a href="#" class="box_selection 0 l0_1B sidebar_l0_1B hover <?php if(get_option_tree('1b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('1b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('1b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 1B</a>
					
					<span class="info_box info_l0_1B hide" style="display: none;" > 
							
					</span>
				</li>
				
				<li><a href="#" class="box_selection 0 l0_2B sidebar_l0_2B hover <?php if(get_option_tree('2b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('2b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('2b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 2B</a>
					
					<span class="info_box info_l0_2B hide" style="display: none;" > 
							
					</span>
				</li>
				<li><a href="#" class="box_selection 0 l0_3B sidebar_l0_3B hover <?php if(get_option_tree('3b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('3b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('3b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 3B</a>
					
					<span class="info_box info_l0_3B hide" style="display: none;" > 
							
					</span>
				</li>
				
				<li><a href="#" class="box_selection 0 l0_4B sidebar_l0_4B hover <?php if(get_option_tree('4b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('4b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('4b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 4B</a>
					
					<span class="info_box info_l0_4B hide" style="display: none;" > 
							
					</span>
				</li>
				
				<li><a href="#" class="box_selection 0 l0_5B sidebar_l0_5B hover <?php if(get_option_tree('5b', '', false, true, 0 ) == "Wolne" ) {
				echo "available"; } ?><?php if(get_option_tree('5b', '', false, true, 0 ) == "Sprzedane" ) {
				echo "sold"; } ?><?php if(get_option_tree('5b', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "reserved"; } ?>">Mieszkanie 5B</a>
					
					<span class="info_box info_l0_5B hide" style="display: none;" > 
							
					</span>
				</li>
				
			</ul>
		</li>
				
</ul>										
					
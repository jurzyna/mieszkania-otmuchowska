jQuery.noConflict();
(function($) {

$(document).ready(function(){

/*----------------------------------------------------*/
// DETECT IF OS IS WINDOWS
/*----------------------------------------------------*/

	if ($.client.os == "Windows"){
		jQuery('html').addClass('win');
	}
	
	
	
/*----------------------------------------------------*/
// RESIZE EMBED VIDEOS
/*----------------------------------------------------*/
	/*
	// SIDEBAR VIDEOS
	$("#sidebar .embed-video iframe, #sidebar .embed-video object").each(function() {
	
		$(this)
		.data('aspectRatio', this.height / this.width)
		.removeAttr('width')
		.removeAttr('height')
		.width('192')
		.height('192' * $(this).data('aspectRatio'));
	});
	
	// FOOTER WIDGETS VIDEOS
	// ONE COLUMN
	$("#footer-wid.one .embed-video iframe, #footer-wid.one .embed-video object").each(function() {
	
		$(this)
		.data('aspectRatio', this.height / this.width)
		.removeAttr('width')
		.removeAttr('height')
		.width('696')
		.height('696' * $(this).data('aspectRatio'));
	});
	
	// TWO COLUMNS
	$("#footer-wid.two .embed-video iframe, #footer-wid.two .embed-video object").each(function() {
	
		$(this)
		.data('aspectRatio', this.height / this.width)
		.removeAttr('width')
		.removeAttr('height')
		.width('333')
		.height('333' * $(this).data('aspectRatio'));
	});
	
	// THREE COLUMNS
	$("#footer-wid.three .embed-video iframe, #footer-wid.three .embed-video object").each(function() {
	
		$(this)
		.data('aspectRatio', this.height / this.width)
		.removeAttr('width')
		.removeAttr('height')
		.width('212')
		.height('212' * $(this).data('aspectRatio'));
	});
	*/
	
/*----------------------------------------------------*/
// FOCUS EFFECT FOR FORM FIELDS
/*----------------------------------------------------*/

	$(".light-skin #branding input").focus(function() {
		$(this)
		.css({backgroundColor: '#ffffff'})
		.animate({ backgroundColor: '#161617', color: '#ffffff' }, 150);
		}).blur(function() {
		if ( $(this).val() == '' || $(this).val() == 'Search...' ){
			$(this).animate({ backgroundColor: '#ffffff', color: '#888888' }, 150);
		}
	});
	
	$(".light-skin #branding aside input").focus(function() {
		$(this)
		.css({backgroundColor: '#eeeeee'})
		.animate({ backgroundColor: '#161617', color: '#ffffff' }, 150);
		}).blur(function() {
		if ( $(this).val() == '' || $(this).val() == 'Search...' ){
			$(this).animate({ backgroundColor: '#eeeeee', color: '#888888' }, 150);
		}
	});
	
	
	$(".dark-skin #branding input").focus(function() {
		$(this)
		.css({backgroundColor: '#161617'})
		.animate({ backgroundColor: '#ffffff', color: '#161617' }, 150);
		}).blur(function() {
		if ( $(this).val() == '' || $(this).val() == 'Search...' ){
			$(this).animate({ backgroundColor: '#161617', color: '#eeeeee' }, 150);
		}
	});
	
	$(".dark-skin #branding aside input").focus(function() {
		$(this)
		.css({backgroundColor: '#333333'})
		.animate({ backgroundColor: '#ffffff', color: '#161617' }, 150);
		}).blur(function() {
		if ( $(this).val() == '' || $(this).val() == 'Search...' ){
			$(this).animate({ backgroundColor: '#333333', color: '#eeeeee' }, 150);
		}
	});

/*----------------------------------------------------*/
// SUPERFISH MENU
/*----------------------------------------------------*/

    $(".main-menu").superfish({
	    delay: 500,
	    hoverClass: 'hover-menu-item',
	    autoArrows: false,
	    animation: {opacity:'show', width:'show'},
		speed: 300
    }); 

/*----------------------------------------------------*/
// MENU HOVER EFFECT
/*----------------------------------------------------*/

	$(".light-skin nav .menu-item").live({
	mouseenter:
		function(){
			
			$(this).children("a:first")
			.css({backgroundColor:'#ffffff', minWidth: '15'})
			.animate({backgroundColor:'#f7f7f7', minWidth: '182'}, 250)
			
		},
	mouseleave:
		function(){
			
			$(this).children("a:first")
			.animate({backgroundColor:'#ffffff', minWidth: '15'}, 250)
			
		}
	});
	
	
	$(".dark-skin nav .menu-item").live({
	mouseenter:
		function(){
			
			$(this).children("a:first")
			.css({backgroundColor:'#161617', minWidth: '15'})
			.animate({backgroundColor:'#000000', minWidth: '182'}, 250)
			
		},
	mouseleave:
		function(){
			
			$(this).children("a:first")
			.animate({backgroundColor:'#161617', minWidth: '15'}, 250)
			
		}
	});

/*----------------------------------------------------*/
// SMOOTH BACK TO TOP
/*----------------------------------------------------*/

	$('a[href=#main_container]').click(function(){
			$('html, body').animate({scrollTop:0}, 'slow');
		return false;
	});

/*----------------------------------------------------*/
// HOVERS & OVERLAYS
/*----------------------------------------------------*/

	/*/// BLOG THUMBS OVERLAY ///*/
	$(".blog-overlay").live({
	mouseenter:
		function(){
			
			$(this).find(".more-hover").stop()
			.css({ right: "-42px", bottom: "-42px", opacity: 0 })
			.animate({ right: "0", bottom: "0", opacity: 1 }, { duration: 250, queue: false });
			
			$(this).find("img").stop()
			.css({ opacity: 1 })
			.animate({ opacity: 0.7 }, { duration: 250, queue: false });
			
		},
	mouseleave:
		function(){
			
			$(this).find(".more-hover").stop().animate({ right: "-42px", bottom: "-42px", opacity: 0 }, { duration: 250, queue: false });
			$(this).find("img").stop().animate({ opacity: 1 }, { duration: 250, queue: false });
			
		}
	});
	
	
	/*/// SQUARE PORTFOLIO THUMBS OVERLAY ///*/
	$(".folio-overlay").live({
	mouseenter:
		function(){
			
			$(this).find(".more-hover").stop()
			.css({ right: "-42px", bottom: "-42px", opacity: 0 })
			.animate({ right: "0", bottom: "0", opacity: 1 }, { duration: 250, queue: false });
			
			$(this).find(".folio-title").stop()
			.css({ opacity: 0 })
			.animate({ opacity: 1 }, { duration: 250, queue: false });
			
			$(this).find("img").stop()
			.css({ opacity: 1 })
			
		},
	mouseleave:
		function(){
			
			$(this).find(".more-hover").stop().animate({ right: "-42px", bottom: "-42px", opacity: 0 }, { duration: 250, queue: false });
			$(this).find(".folio-title").stop().animate({ opacity: 0 }, { duration: 250, queue: false });
			
		}
	});
	
	
	/*/// 1-COL PORTFOLIO THUMBS OVERLAY ///*/
	$(".page-template-portfolio-1col-php .folio-overlay, .page-template-portfolio-1col-cat-php .folio-overlay").live({
	mouseenter:
		function(){
			
			$(this).find(".more-hover").stop()
			.css({ right: "-42px", bottom: "-42px", opacity: 0 })
			.animate({ right: "0", bottom: "0", opacity: 1 }, { duration: 250, queue: false });
			
			$(this).find("img").stop()
			.css({ opacity: 1 })
			.animate({ opacity: 0.7 }, { duration: 250, queue: false });
			
		},
	mouseleave:
		function(){
			
			$(this).find(".more-hover").stop().animate({ right: "-42px", bottom: "-42px", opacity: 0 }, { duration: 250, queue: false });
			$(this).find("img").stop().animate({ opacity: 1 }, { duration: 250, queue: false });
			
		}
	});



/*----------------------------------------------------*/
// EQUAL HEIGHT COLUMNS
/*----------------------------------------------------*/

    equalHeight('.recent-posts > li');
    

}); // END $(document).ready


var maxHeight = 0;
function equalHeight(column) {
    
    column = $(column);
    
    column.each(function() {       
        
        if($(this).height() > maxHeight) {
            maxHeight = $(this).height();;
        }
    });
    
    column.height(maxHeight);
}

/*----------------------------------------------------*/
// CHECK IE VERSION & ADD CLASSES
/*----------------------------------------------------*/

if (jQuery.browser.msie && jQuery.browser.version == '8.0' ) {
	jQuery('html').addClass('ie8');
}

if (jQuery.browser.msie && jQuery.browser.version == '9.0' ) {
	jQuery('html').addClass('ie9');
}

/*----------------------------------------------------*/
// CLEAR FORM FIELDS ON FOCUS
/*----------------------------------------------------*/

$(function() {
	$('input:text, textarea').each(function() {
	var field = $(this);
	var default_value = field.val();
	field.focus(function() {
	if (field.val() == default_value) {
	field.val('');
	}
	});
	field.blur(function() {
	if (field.val() == '') {
	field.val(default_value);
	}
	});
	});
});

/***************************************
			Mouse Hover Events
	*****************************************/
	$(".box_selection").hover(
	  function () {
	  	var selection = $(this).attr('class').split(" ")[2];
	  	var divBox = '#' + selection;
	  	var divNav = '.sidebar_' + selection;
	  		
	  		if($(this).hasClass('hover')){
	  			//alert(divBox);
	  		   $(divBox).stop().fadeTo(200, 1);
	  		   if($(divNav).hasClass('available')){
		  		   $(divNav).animate({
		  		         'background-color': '#007167',
		  		         'color': '#fff'
		  		     }, 200 );
	  		     }
	  		     if($(divNav).hasClass('sold')){
	  		     	   $(divNav).animate({
	  		     	         'background-color': '#8a2e2e',
	  		     	         'color': '#fff'
	  		     	     }, 200 );
	  		       }
	  		  if($(divNav).hasClass('reserved')){
	  		  	   $(divNav).animate({
	  		  	         'background-color': '#125F8A',
	  		  	         'color': '#fff'
	  		  	     }, 200 );
	  		    }
	  		}
	  		
	  },
	  function () {
		var selection = $(this).attr('class').split(" ")[2];
		var divBox = '#' + selection;
		var divNav = '.sidebar_' + selection;
		
			
			if($(this).hasClass('hover')){
			   $(divBox).stop().fadeTo(200, 0);
			   $(divNav).animate({
			         'background-color': '#fff',
			         'color': '#000'
			     }, 200 );
			}
	  }
	);  
	
	  
	        	
	/***************************************
		Mouse Click Events
*****************************************/
	
	
	$('.box_selection').click(function() {
	    	  var selection = $(this).attr('class').split(" ")[2];
	    	  var divBox = '#' + selection;
	    	  var divNav = '.sidebar_' + selection;
	    	  var divMap = 'area.' + selection;
	    	  var infoSide = '.info_' + selection;
	    	  var infoCheck = 'info_' + selection;
	    	  //var infoSide = '.plany_infoSidebar';
	    	  var loadInfo = 'test/plany-info/ ' + infoSide;
	    	  
	    	  //if($(this).hasClass('hover')){
	    	      	  	  $('.box_selection').not(divMap  + "," + divNav).removeClass("clicked").addClass("hover");
	    	      	  	  
	    	      	  	  $('a.box_selection').not(divNav).animate({
	    	      	  	          'background-color': '#fff',
	    	      	  	          'color': '#000'
	    	      	  	      }, 200);
	    	      	  	  
	    	      	  	  $('ul.group_list li').removeClass('selected').stop().fadeTo(200, 0);
	    	      	  	  
	    	      	  	  $(divMap  + "," + divNav).addClass('clicked').toggleClass('hover');
	    	      	  	  
	    	      	  	  $(divBox).toggleClass('selected').stop().fadeTo(200, 1);
	    	      	  	
							if($(divNav).hasClass('available')){
    	    	      	  	  $(divNav).animate({
    	    	      	  	              'background-color': '#007167',
    	    	      	  	              'color': '#fff'
    	    	      	  	          }, 200);
	    	  				}
	    	  				
	    	      	    //}
	    	      	    //$('.plany_infoSidebar').load('test/plany-info/ .info_l0_1C');
	    	      	    //alert(infoSide);
	    	      	    
	    	      	   if($('#informacja').hasClass(infoCheck)){

	    	      	    }
	    	      	    else {
	    	      	    showLoading();
	    	      	    

	    	      	    	$('#informacja').remove();
	    	      	    	showLoading();
	    	      	    	//$('#infoLoader').load(loadInfo, hideLoading);
	    	      	    	
	    	      	    	$('#infoLoader').load(loadInfo, hideLoading, function() {     
	    	      	    	                $(".imagePretty").prettyPhoto().click();
	    	      	    	                $.prettyPhoto.open('http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/1-1D.jpg','Title','Description');    
	    	      	    	                });
	    	      	    }
	    	      	
				function showLoading(){
					$('.loader').css({visibility:"visible"}).css({opacity:"1"});
				}
				
				function hideLoading(){
					$('.loader').fadeTo(1000, 0);
				}
	    	    
	    	  });
	
	
		
	
	$(".box_selection, .box_selection_level").click(function(){
	 	var nameId = $(this).attr('class').split(" ")[1];
	    var newClass = "#l" + nameId;
	    var useMap = '#map' + (nameId);
	    var levelTitle = '.level_title_' + (nameId);
	    var levelBg = "#l" + nameId + "_front_bg";
	    
	   	if($(newClass).prev().hasClass('down')){
	    	$(newClass).prevAll(".down").animate({"top": "-=120px"}, "slow").toggleClass("up").removeClass("down");
	    }
	        
	   	else if($(newClass).hasClass('up')){
	    	$(newClass).animate({"top": "+=120px"}, "slow").toggleClass("down").removeClass("up");
	    	$(newClass).nextAll(".up").animate({"top": "+=120px"}, "slow").toggleClass("down").removeClass("up");
	    					         	
	       }
	       
	    if($(levelTitle).hasClass('down')){
	    	$('.group_list_side').slideUp('fast');
	    	$('.group_list_side').siblings("a").addClass("down").removeClass("up");
	    	$(levelTitle).siblings("ul").slideDown('fast');
	    	$(levelTitle).toggleClass("up").removeClass("down");
	    }
	    
	    if ( $('#l7').hasClass("up") && $('#l6').hasClass("down") ) {
	    	$('#l7_front_bg').addClass('itselfup');
	    } else {
	    	$('#l7_front_bg').removeClass('itselfup');
	    }
	    /*
	    //alert(levelBg);
	    $('.front_bg').removeClass("current")
	    $(levelBg).addClass("current"); */
	    $("#transparent_full").attr('usemap', (useMap));
	   
	   	}); 



})(jQuery);
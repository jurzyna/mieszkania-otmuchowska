<?php
/*
Template Name: Plany 2
*/
?>
<?php get_header();?>
	
	<section id="content-wrap">
		<div id="main">
			
			<?php the_post(); ?>
			
			<article id="post-<?php the_ID();?>" <?php post_class('entry-wrap');?>>
				<?php if ( has_post_thumbnail()): ?>
				<div class="entry-thumb">
					<?php the_post_thumbnail('fullwidth-page-image'); ?>
				</div>
				<?php endif; ?>
				
				<h1 class="entry-title"><?php the_title(); ?></h1>
				

				
				<div class="entry-content">
					<div id="content_left_all">

					<div class="content_left plany_relative">
						<div id="link_image">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/selector/img/transparent.png" id="transparent_full" alt="" usemap="#map3" height="375px" width="375px"/>
						</div>
						<?php get_template_part( 'selector/plany' ); ?>
					</div>		

					<div class="plany_nav content_left">
						<?php get_template_part( 'selector/bottom', 'navigation'); ?>
					</div>
					
					</div>
					<div class="plany_infoSidebar content_left">

						<div id="infoLoader"></div>
						<span class="loader" style="opacity: 0"></span>
						<span class="legenda">
						<h5>Legenda</h5>
						<span class="legendAvailable">Wolne</span>
						<span class="legendaSold">Sprzedane</span>
						<span class="legendReserved">Rezerwacja</span>
						</span>
					</div>
					
					
					<div class="clear"></div>					

				
				</div>
			</article>
			
			<div id="maps" style="display: none;">
	
			<?php //get_template_part( 'selector/image-maps/map', '3' ); ?>
			<?php //get_template_part( 'selector/image-maps/map', '2' ); ?>
			<?php //get_template_part( 'selector/image-maps/map', '1' ); ?>
			<?php //get_template_part( 'selector/image-maps/map', '0' ); ?>
											
			</div>

	
</div><!-- #main -->

<?php get_footer();?>
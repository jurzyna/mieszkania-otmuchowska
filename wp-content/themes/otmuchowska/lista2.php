<?php
/*
Template Name: Lista 2
*/
?>
<?php get_header();?>
	
	<section id="content-wrap">
		<div id="main">
			
			<?php the_post(); ?>
			
			<article id="post-<?php the_ID();?>" <?php post_class('entry-wrap');?>>
				<?php if ( has_post_thumbnail()): ?>
				<div class="entry-thumb">
					<?php the_post_thumbnail('fullwidth-page-image'); ?>
				</div>
				<?php endif; ?>
				
				<h1 class="entry-title"><?php the_title(); ?></h1>
				
				<div class="entry-content">
					<div class="entry-content">
										<div class="tabs ui-tabs ui-widget ui-widget-content ui-corner-all"><ul class="tabset ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"><li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tab-0" class="tab">Klatka 54A</a></li><li class="ui-state-default ui-corner-top"><a href="#tab-1" class="tab">Klatka 54B</a></li><li class="clear"></li></ul><div id="tab-0" class="tabContent ui-tabs-panel ui-widget-content ui-corner-bottom" style=""><p><div class="one_fifth_lista content_left listaTitle"><p>NUMER MIESZKANIA</p></div>
										<div class="one_fifth_lista content_left listaTitle"><p>LOKALIZACJA</p></div>
										<div class="one_fifth_lista content_left listaTitle"><p>POWIERZCHNIA</p></div>				
										<div class="one_fifth_lista content_left listaTitle"><p>BALKON</p></div>
										<div class="one_fifth_lista_last content_left listaTitle"><p>STATUS</p></div><div class="<?php if(get_option_tree('1a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('1a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('1a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>1A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>33,55</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('1a')) { ?><p><?php echo get_option_tree('1a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('2a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('2a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('2a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>2A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>107,28</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>2</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('2a')) { ?><p><?php echo get_option_tree('2a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('3a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('3a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('3a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>3A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>67,5</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('3a')) { ?><p><?php echo get_option_tree('3a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('4a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('4a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('4a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>4A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>33,2</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>2</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('4a')) { ?><p><?php echo get_option_tree('4a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('5a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('5a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('5a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>5A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>76,43</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>brak</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('5a')) { ?><p><?php echo get_option_tree('5a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('6a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('6a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('6a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>6A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>85,77</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('6a')) { ?><p><?php echo get_option_tree('6a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('7a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('7a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('7a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>7A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>33,55</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('7a')) { ?><p><?php echo get_option_tree('7a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('8a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('8a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('8a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>8A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>107,28</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>2</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('8a')) { ?><p><?php echo get_option_tree('8a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('9a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('9a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('9a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>9A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>67,65</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('9a')) { ?><p><?php echo get_option_tree('9a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('10a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('10a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('10a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>10A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>33,20</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('10a')) { ?><p><?php echo get_option_tree('10a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('11a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('11a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('11a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>11A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>76,43</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>brak</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('11a')) { ?><p><?php echo get_option_tree('11a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('12a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('12a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('12a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>12A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>85,77</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('12a')) { ?><p><?php echo get_option_tree('12a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('13a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('13a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('13a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>13A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>33,55</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('13a')) { ?><p><?php echo get_option_tree('13a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('14a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('14a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('14a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>14A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>107,28</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>2</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('14a')) { ?><p><?php echo get_option_tree('14a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('15a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('15a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('15a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>15A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>67,65</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('15a')) { ?><p><?php echo get_option_tree('15a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('16a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('16a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('16a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>16A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>33,20</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('16a')) { ?><p><?php echo get_option_tree('16a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('17a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('17a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('17a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>17A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>76,43</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>brak</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('17a')) { ?><p><?php echo get_option_tree('17a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('18a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('18a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('18a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>18A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>85,77</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('18a')) { ?><p><?php echo get_option_tree('18a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('19a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('19a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('19a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>19A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>62,43</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>2</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('19a')) { ?><p><?php echo get_option_tree('19a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('20a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('20a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('20a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>20A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>49,58</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('20a')) { ?><p><?php echo get_option_tree('20a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('21a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('21a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('21a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>21A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>52,37</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('21a')) { ?><p><?php echo get_option_tree('21a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('22a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('22a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('22a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>22A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>26,24</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('22a')) { ?><p><?php echo get_option_tree('22a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('23a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('23a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('23a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>23A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>62,85</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('23a')) { ?><p><?php echo get_option_tree('23a'); ?></p><?php } ?></div>
										</div>
										
										<div class="<?php if(get_option_tree('24a', '', false, true, 0 ) == "Wolne" ) {
										echo "listaFree"; } ?><?php if(get_option_tree('24a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "listaSold"; } ?><?php if(get_option_tree('24a', '', false, true, 0 ) == "Rezerwacja" ) {
										echo "listaReserved"; } ?>">					
											<div class="one_fifth_lista content_left listaInfo"><p>24A</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
											<div class="one_fifth_lista content_left listaInfo"><p>62,75</p></div>				
											<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
											<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('24a')) { ?><p><?php echo get_option_tree('24a'); ?></p><?php } ?></div>
										</div>
										</p>
					</div><div id="tab-1" class="tabContent ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" style=""><p><div class="one_fifth_lista content_left listaTitle"><p>NUMER MIESZKANIA</p></div>
						<div class="one_fifth_lista content_left listaTitle"><p>LOKALIZACJA</p></div>
						<div class="one_fifth_lista content_left listaTitle"><p>POWIERZCHNIA</p></div>				
						<div class="one_fifth_lista content_left listaTitle"><p>BALKON</p></div>
						<div class="one_fifth_lista_last content_left listaTitle"><p>STATUS</p></div>
						
					<div class="<?php if(get_option_tree('1b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('1b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('1b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>1B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>85,67</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>2</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('1b')) { ?><p><?php echo get_option_tree('1b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('2b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('2b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('2b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>2B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>75,72</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('2b')) { ?><p><?php echo get_option_tree('2b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('3b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('3b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('3b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>3B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>51,76</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('3b')) { ?><p><?php echo get_option_tree('3b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('4b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('4b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('4b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>4B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>60,72</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('4b')) { ?><p><?php echo get_option_tree('4b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('5b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('5b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('5b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>5B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>50,78</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('5b')) { ?><p><?php echo get_option_tree('5b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('6b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('6b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('6b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>6B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>85,67</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>2</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('6b')) { ?><p><?php echo get_option_tree('6b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('7b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('7b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('7b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>7B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>75,72</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('7b')) { ?><p><?php echo get_option_tree('7b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('8b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('8b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('8b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>8B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>51,76</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('8b')) { ?><p><?php echo get_option_tree('8b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('9b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('9b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('9b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>9B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>60,72</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('9b')) { ?><p><?php echo get_option_tree('9b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('10b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('10b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('10b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>10B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>50,78</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('10b')) { ?><p><?php echo get_option_tree('10b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('11b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('11b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('11b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>11B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>85,67</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>2</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('4b')) { ?><p><?php echo get_option_tree('11b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('12b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('12b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('12b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>12B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>75,72</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('12b')) { ?><p><?php echo get_option_tree('12b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('13b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('13b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('13b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>13B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>51,76</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('13b')) { ?><p><?php echo get_option_tree('13b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('14b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('14b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('14b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>14B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>60,72</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('14b')) { ?><p><?php echo get_option_tree('14b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('15b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('15b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('15b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>15B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>50,78</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('15b')) { ?><p><?php echo get_option_tree('15b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('16b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('16b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('16b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>16B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>61,75</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>2</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('16b')) { ?><p><?php echo get_option_tree('16b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('17b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('17b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('17b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>17B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>63,42</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>brak</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('17b')) { ?><p><?php echo get_option_tree('17b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('18b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('18b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('18b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>18B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>40,16</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('18b')) { ?><p><?php echo get_option_tree('18b'); ?></p><?php } ?></div>
					</div>
					
					<div class="<?php if(get_option_tree('19b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('19b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('19b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>19B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>48,01</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('19b')) { ?><p><?php echo get_option_tree('19b'); ?></p><?php } ?></div>
					</div>
					
					
					<div class="<?php if(get_option_tree('20b', '', false, true, 0 ) == "Wolne" ) {
					echo "listaFree"; } ?><?php if(get_option_tree('20b', '', false, true, 0 ) == "Sprzedane" ) {
					echo "listaSold"; } ?><?php if(get_option_tree('20b', '', false, true, 0 ) == "Rezerwacja" ) {
					echo "listaReserved"; } ?>">					
						<div class="one_fifth_lista content_left listaInfo"><p>20B</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
						<div class="one_fifth_lista content_left listaInfo"><p>36,14</p></div>				
						<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
						<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('20b')) { ?><p><?php echo get_option_tree('20b'); ?></p><?php } ?></div>
					</div>
					
										
					
							</p>
					</div></div>					
															
					
									</div>
					
					<?php wp_link_pages( array( 'before' => '<p><span>' . __( 'Pages:', 'premitheme' ) . '</span>', 'after' => '</p>' ) ); ?>
					

				</div>
			</article>
			
		</div><!-- #main -->
		
<?php get_footer();?>
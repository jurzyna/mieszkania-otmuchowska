<?php
$crop = of_get_option('crop_location');
$itemDate = get_post_meta($post->ID, 'folioDate', TRUE);
$prevVid = get_post_meta($post->ID, 'prevVid', TRUE);
$prevVidEmbed = get_post_meta($post->ID, 'prevVidEmbed', TRUE);
$prevHeight = get_post_meta($post->ID, 'prevHeight', TRUE);
$folioClient = get_post_meta($post->ID, 'folioClient', TRUE);
$folioUrl = get_post_meta($post->ID, 'folioUrl', TRUE);
$prevImages = get_post_meta($post->ID, 'prevImg', TRUE);

$folio_cats =  get_the_terms( get_the_ID(), 'portfolio_cats' ); 
$cats_names = array();
if( !empty($folio_cats) ):
	foreach( $folio_cats as $folio_cat ):
		$cats_names[] = $folio_cat->name;
	endforeach; 
	$cat_name = join( ', ', $cats_names );
endif;

$folio_skills =  get_the_terms( get_the_ID(), 'portfolio_skills' ); 
if ( $folio_skills && ! is_wp_error( $folio_skills ) ):
	$skills_names = array();
	foreach( $folio_skills as $folio_skill ):
		$skills_names[] = $folio_skill->name;
	endforeach; 
endif;
?>
<?php get_header();?>
	
	<section id="content-wrap">
		<div id="main">
		<article id="portfolio_grid" class="post-139 page type-page status-publish hentry entry-wrap three-col">
			
			<div id="portfolio-header">
								
								<h1 class="entry-title"><?php the_title(); ?></h1>

			</div>
			
			<?php while ( have_posts() ) : the_post(); ?>
			
			
				<div id="previews-wrap">
					<?php if( $prevVidEmbed ):
          $embed_code = htmlspecialchars_decode($prevVidEmbed);
  				?>
  				<div id="vid-preview"><?php echo $embed_code; ?></div>

					<?php elseif( $prevVid ):
					$embed_code = wp_oembed_get($prevVid, array('width'=>726));
					?>
					<div id="vid-preview"><?php echo $embed_code; ?></div>
					
					<?php elseif (count($prevImages) > 0): ?>
					<?php if (count($prevImages) > 1): ?>
					
					<ul id="folio-items">
						<?php foreach((array)$prevImages as $prevImg ):
							
							$prevImgUrl = pt_get_image_path($prevImg);
							
							echo '<li class="digital-painting all folio-wrap folio-thumb"><a class="folio-overlay" title="" href="'.$prevImgUrl.'" rel="prettyPhoto[group-'.get_the_ID().']"><img src="'.PT_FUNCTIONS.'/timthumb.php?src='.$prevImgUrl.'&amp;h='.$prevHeight.'&amp;w=240&amp;zc=1&amp;q=100&amp;a='.$crop.'" alt=""/>
							<div class="folio-title" style="opacity: 0; ">

															</div>
							<span class="more-hover"></span>
							</a></li>';
						endforeach; ?>
						
						
						
					</ul>
					
					<?php else: ?>

					<?php endif; ?>
					<?php endif; ?>
				</div>
			
			<?php endwhile; ?>
			</article>
			
		</div><!-- #main -->
		
<?php get_footer();?>
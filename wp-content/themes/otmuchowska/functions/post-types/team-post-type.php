<?php
/*-----------------------------/
	REGISTER POST TYPE
/-----------------------------*/



/*-----------------------------/
	EDIT LIST CUSTOM COLUMNS
/-----------------------------*/
add_filter("manage_edit-team_columns", "team_edit_columns");

add_action("manage_posts_custom_column",  "team_columns_display", 10, 2);

add_theme_support( 'post-thumbnails', array( 'team' ) );
   
function team_edit_columns($team_columns){
    $team_columns = array(
        "cb" => "<input type=\"checkbox\" />",
        "title" => _x('Member', 'column name', 'premitheme'),
        "team_thumbnail" => __('Thumbnail', 'premitheme'),
        "author" => __('Author', 'premitheme'),
        "date" => __('Date', 'premitheme'),
    );
    return $team_columns;
}

function team_columns_display($team_columns, $post_id){
    switch ($team_columns)
    {
        // Code from: http://wpengineer.com/display-post-thumbnail-post-page-overview
            
        case "team_thumbnail":
            $thumb_src = get_post_meta($post_id, 'memberImgURL', TRUE);
            $thumb_id = pt_get_attachment_id_by_src($thumb_src);
                
            if ($thumb_id != ''){
                $thumb = wp_get_attachment_image( $thumb_id, '100x100', true );
                echo $thumb;
            } else {
                echo __('None', 'premitheme');
            }
            
         break;
    }
}



/*-----------------------------------------/
	CHANGE TITLE FIELD PLACEHOLDER TEXT
/-----------------------------------------*/
function team_title_text( $title ){
	$screen = get_current_screen();
	if ( 'team' == $screen->post_type ) {
		$title = "Enter member's name here";
	}
	return $title;
}
add_filter( 'enter_title_here', 'team_title_text' );

<?php
/*
Template Name: Plany
*/
?>
<?php get_header();?>
	
	<section id="content-wrap">
		<div id="main">
			
			<?php the_post(); ?>
			
			<article id="post-<?php the_ID();?>" <?php post_class('entry-wrap');?>>
				<?php if ( has_post_thumbnail()): ?>
				<div class="entry-thumb">
					<?php the_post_thumbnail('fullwidth-page-image'); ?>
				</div>
				<?php endif; ?>
				
				<h1 class="entry-title"><?php the_title(); ?></h1>
				

				
				<div class="entry-content">
					<div id="content_left_all">
					<div class="content_left plany_relative">
					
						<div id="link_image">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/selector/img/transparent.png" id="transparent_full" alt="" usemap="#map3" height="375px" width="375px"/>
						  </div>
								
									
									<!--<div id="reuss" class="group">
										Reuss
									</div>
									
									<div id="quartierplatz" class="group">
										Quartierplatz
									</div> -->
						
						            <div id="l3" class="group down">
										
										<div class="level_inner inner">
											<div id="l3_back_bg"></div>
											<div id="l3_lines_bg"></div>
											<ul class="group_list">
						                        <li id="l3_16C" style="display: none;" class="<?php if(get_option_tree('16c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('16c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('16c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l3_17C" style="display: none;" class="<?php if(get_option_tree('17c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('17c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('17c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l3_18C" style="display: none;" class="<?php if(get_option_tree('18c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('18c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('18c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l3_19C" style="display: none;" class="<?php if(get_option_tree('19c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('19c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('19c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l3_19D" style="display: none;" class="<?php if(get_option_tree('19d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('19d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('19d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l3_20D" style="display: none;" class="<?php if(get_option_tree('20d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('20d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('20d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l3_21D" style="display: none;" class="<?php if(get_option_tree('21d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('21d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('21d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l3_22D" style="display: none;" class="<?php if(get_option_tree('22d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('22d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('22d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
											</ul>
											<div id="l3_front_bg" class="front_bg"></div>
												
										</div>
									</div>
						            
						           
						            <div id="l2" class="group down">
										
										<div class="level_inner inner">
											<div id="l2_back_bg"></div>
											<div id="l2_lines_bg"></div>
											<ul class="group_list">
						                        <li id="l2_12C" style="display: none;" class="<?php if(get_option_tree('12c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('12c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('12c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l2_11C" style="display: none;" class="<?php if(get_option_tree('11c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('11c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('11c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l2_13C" style="display: none;" class="<?php if(get_option_tree('13c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('13c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('13c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l2_14C" style="display: none;" class="<?php if(get_option_tree('14c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('14c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('14c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l2_15C" style="display: none;" class="<?php if(get_option_tree('18c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('18c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('18c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l2_13D" style="display: none;" class="<?php if(get_option_tree('13d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('13d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('13d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l2_14D" style="display: none;" class="<?php if(get_option_tree('14d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('14d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('14d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l2_15D" style="display: none;" class="<?php if(get_option_tree('15d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('15d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('15d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l2_16D" style="display: none;" class="<?php if(get_option_tree('16d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('16d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('16d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l2_17D" style="display: none;" class="<?php if(get_option_tree('17d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('17d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('17d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l2_18D" style="display: none;" class="<?php if(get_option_tree('18d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('18d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('18d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
											</ul>
											<div id="l2_front_bg" class="front_bg"></div>
												
										</div>
									</div>
									 
						            
									<div id="l1" class="group down">
										
										<div class="level_inner inner">
											<div id="l1_back_bg"></div>
											<div id="l1_lines_bg"></div>
											<ul class="group_list">
						                        <li id="l1_6C" style="display: none;" class="<?php if(get_option_tree('6c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('6c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('6c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l1_7C" style="display: none;" class="<?php if(get_option_tree('7c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('7c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('7c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l1_8C" style="display: none;" class="<?php if(get_option_tree('8c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('8c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('8c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l1_9C" style="display: none;" class="<?php if(get_option_tree('9c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('9c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('9c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>>"><a href="#"></a></li>
						                        <li id="l1_10C" style="display: none;" class="<?php if(get_option_tree('10c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('10c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('10c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l1_7D" style="display: none;" class="<?php if(get_option_tree('7d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('7d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('7d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l1_8D" style="display: none;" class="<?php if(get_option_tree('8d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('8d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('8d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l1_9D" style="display: none;" class="<?php if(get_option_tree('9d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('9d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('9d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l1_10D" style="display: none;" class="<?php if(get_option_tree('10d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('10d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('10d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l1_11D" style="display: none;" class="<?php if(get_option_tree('11d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('11d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('11d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l1_12D" style="display: none;" class="<?php if(get_option_tree('12d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('12d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('12d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
											</ul>
											<div id="l1_front_bg" class="front_bg"></div>
												
										</div>
									</div>
						
									
									<div id="l0" class="group down">
										
										<div class="level_inner inner">
											<div id="l0_back_bg"></div>
											<div id="l0_lines_bg"></div>
											<ul class="group_list">
												<li id="l0_1C" style="display: none;" class="<?php if(get_option_tree('1c', '', false, true, 0 ) == "Wolne" ) {
												echo "free"; } ?><?php if(get_option_tree('1c', '', false, true, 0 ) == "Sprzedane" ) {
												echo "sold"; } ?><?php if(get_option_tree('1c', '', false, true, 0 ) == "Rezerwacja" ) {
												echo "reserved"; } ?>"><a href="#"></a></li>	
						                        <li id="l0_2C" style="display: none;" class="<?php if(get_option_tree('2c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('2c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('2c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l0_3C" style="display: none;" class="<?php if(get_option_tree('3c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('3c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('3c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l0_4C" style="display: none;" class="<?php if(get_option_tree('4c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('4c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('4c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l0_5C" style="display: none;" class="<?php if(get_option_tree('5c', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('5c', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('5c', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l0_1D" style="display: none;" class="<?php if(get_option_tree('1d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('1d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('1d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l0_2D" style="display: none;" class="<?php if(get_option_tree('2d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('2d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('2d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l0_3D" style="display: none;" class="<?php if(get_option_tree('3d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('3d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('3d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l0_4D" style="display: none;" class="<?php if(get_option_tree('4d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('4d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('4d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l0_5D" style="display: none;" class="<?php if(get_option_tree('5d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('5d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('5d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
						                        <li id="l0_6D" style="display: none;" class="<?php if(get_option_tree('6d', '', false, true, 0 ) == "Wolne" ) {
						                        echo "free"; } ?><?php if(get_option_tree('6d', '', false, true, 0 ) == "Sprzedane" ) {
						                        echo "sold"; } ?><?php if(get_option_tree('6d', '', false, true, 0 ) == "Rezerwacja" ) {
						                        echo "reserved"; } ?>"><a href="#"></a></li>
											</ul>
											<div id="l0_front_bg" class="front_bg"></div>
												
										</div>
									</div>
				
					</div>
					
					<div class="plany_nav content_left">
										
											<ul id="accordion">
																<li class="klatka_title">
																	Klatka 54C
																</li>
												  				
												  				<li><a href="#" class="box_selection_level 3 level_title_3 level_title down">Poddasze</a>
												  					
												  					<ul class="group_list_side" style="display: none;">
												  						
												  						<li><a href="#" class="box_selection 3 l3_16C sidebar_l3_16C hover <?php if(get_option_tree('16c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('16c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('16C', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 16C</a>
												  							<span class="info_box info_l3_16C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 3 l3_17C sidebar_l3_17C hover <?php if(get_option_tree('17c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('17c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('17c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 17C</a>
												  							
												  							<span class="info_box info_l3_17C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 3 l3_18C sidebar_l3_18C hover <?php if(get_option_tree('18c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('18c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('18c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 18C</a>
												  							
												  							<span class="info_box info_l3_18C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 3 l3_19C sidebar_l3_19C hover <?php if(get_option_tree('19c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('19c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('19c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 19C</a>
												  							
												  							<span class="info_box info_l3_19C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  					</ul>
												  				</li>
											
																<li><a href="#" class="box_selection_level 2 level_title_2 level_title down">II Piętro</a>
																	
																	<ul class="group_list_side" style="display: none;">
																	
																		<li><a href="#" class="box_selection 2 l2_11C sidebar_l2_11C hover <?php if(get_option_tree('11C', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('11c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('11c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 11C</a>
																			
																			<span class="info_box info_l2_11C hide" style="display: none;" > 
																					
																			</span>
																		</li>
																		
																		<li><a href="#" class="box_selection 2 l2_12C sidebar_l2_12C hover <?php if(get_option_tree('12c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('12c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('12c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 12C</a>
																			
																			<span class="info_box info_l2_12C hide" style="display: none;" > 
																					
																			</span>
																		</li>
																		
																		<li><a href="#" class="box_selection 2 l2_13C sidebar_l2_13C hover <?php if(get_option_tree('13c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('13c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('13c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 13C</a>
																			
																			<span class="info_box info_l2_13C hide" style="display: none;" > 
																					
																			</span>
																		</li>
																		
																		<li><a href="#" class="box_selection 2 l2_14C sidebar_l2_14C hover <?php if(get_option_tree('14c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('14c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('14c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 14C</a>
																			
																			<span class="info_box info_l2_14C hide" style="display: none;" > 
																					
																			</span>
																		</li>
																		
																		<li><a href="#" class="box_selection 2 l2_15C sidebar_l2_15C hover <?php if(get_option_tree('15c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('15c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('15c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 15C</a>
																			
																			<span class="info_box info_l2_15C hide" style="display: none;" > 
																					
																			</span>
																		</li>
																															
																	</ul>
																</li>						  				
												  				
												  				<li><a href="#" class="box_selection_level 1 level_title_1 level_title down">I Piętro</a>
												  					
												  					<ul class="group_list_side" style="display: none;">
												  						
												  						<li><a href="#" class="box_selection 1 l1_6C sidebar_l1_6C hover <?php if(get_option_tree('6c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('6c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('6c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 6C</a>
												  							
												  							<span class="info_box info_l1_6C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 1 l1_7C sidebar_l1_7C hover <?php if(get_option_tree('7c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('7c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('7c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 7C</a>
												  							
												  							<span class="info_box info_l1_7C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 1 l1_8C sidebar_l1_8C hover <?php if(get_option_tree('8c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('8c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('8c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 8C</a>
												  							
												  							<span class="info_box info_l1_8C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 1 l1_9C sidebar_l1_9C hover <?php if(get_option_tree('9c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('9c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('9c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 9C</a>
												  							
												  							<span class="info_box info_l1_9C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 1 l1_10C sidebar_l1_10C hover <?php if(get_option_tree('10c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('10c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('10c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 10C</a>
												  							
												  							<span class="info_box info_l1_10C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  					</ul>
												  				</li>
												  				
												  				<li><a href="#" class="box_selection_level 0 level_title_0 level_title down">Parter</a>
												  					
												  					<ul class="group_list_side" style="display: none;">
												  						
												  						<li><a href="#" class="box_selection 0 l0_1C sidebar_l0_1C hover <?php if(get_option_tree('1c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('1c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('1c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 1C</a>
												  							
												  							<span class="info_box info_l0_1C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 0 l0_2C sidebar_l0_2C hover <?php if(get_option_tree('2c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('2c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('2c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 2C</a>
												  							
												  							<span class="info_box info_l0_2C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 0 l0_3C sidebar_l0_3C hover <?php if(get_option_tree('3c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('3c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('3c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 3C</a>
												  							
												  							<span class="info_box info_l0_3C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 0 l0_4C sidebar_l0_4C hover <?php if(get_option_tree('4c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('4c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('4c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 4C</a>
												  							
												  							<span class="info_box info_l0_4C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  						
												  						<li><a href="#" class="box_selection 0 l0_5C sidebar_l0_5C hover <?php if(get_option_tree('5c', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('5c', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('5c', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 5C</a>
												  							
												  							<span class="info_box info_l0_5C hide" style="display: none;" > 
												  									
												  							</span>
												  						</li>
												  													  					</ul>
												  				</li>
												  				
											  		</ul>
										
													<ul id="accordion2">
																		<li class="klatka_title">
																			Klatka 54D
																		</li>
														  				
														  				<li><a href="#" class="box_selection_level 3 level_title_3 level_title down">Poddasze</a>
														  					
														  					<ul class="group_list_side" style="display: none;">
														  						
														  															  						
														  						<li><a href="#" class="box_selection 3 l3_19D sidebar_l3_19D hover <?php if(get_option_tree('19d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('19d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('19d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 19D</a>
														  							
														  							<span class="info_box info_l3_19D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 3 l3_20D sidebar_l3_20D hover <?php if(get_option_tree('20d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('20d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('20d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 20D</a>
														  							
														  							<span class="info_box info_l3_20D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 3 l3_21D sidebar_l3_21D hover <?php if(get_option_tree('21d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('21d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('21d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 21D</a>
														  							
														  							<span class="info_box info_l3_21D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 3 l3_22D sidebar_l3_22D hover <?php if(get_option_tree('22d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('22d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('22d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 22D</a>
														  							
														  							<span class="info_box info_l3_22D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  					</ul>
														  				</li>
																		
																		
																		<li><a href="#" class="box_selection_level 2 level_title_2 level_title down">II Piętro</a>
																			
																			<ul class="group_list_side" style="display: none;">
																			
																				
																				<li><a href="#" class="box_selection 2 l2_13D sidebar_l2_13D hover <?php if(get_option_tree('13d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('13d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('13d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 13D</a>
																					
																					<span class="info_box info_l2_13D hide" style="display: none;" > 
																							
																					</span>
																				</li>
																				
																				<li><a href="#" class="box_selection 2 l2_14D sidebar_l2_14D hover <?php if(get_option_tree('14d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('14d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('14d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 14D</a>
																					
																					<span class="info_box info_l2_14D hide" style="display: none;" > 
																							
																					</span>
																				</li>
																				
																				<li><a href="#" class="box_selection 2 l2_15D sidebar_l2_15D hover <?php if(get_option_tree('15d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('15d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('15d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 15D</a>
																					
																					<span class="info_box info_l2_15D hide" style="display: none;" > 
																							
																					</span>
																				</li>
																				
																				<li><a href="#" class="box_selection 2 l2_16D sidebar_l2_16D hover <?php if(get_option_tree('16d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('16d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('16d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 16D</a>
																					
																					<span class="info_box info_l2_16D hide" style="display: none;" > 
																							
																					</span>
																				</li>
																				
																				<li><a href="#" class="box_selection 2 l2_17D sidebar_l2_17D hover <?php if(get_option_tree('17d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('17d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('17d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 17D</a>
																					
																					<span class="info_box info_l2_17D hide" style="display: none;" > 
																							
																					</span>
																				</li>
																				
																				<li><a href="#" class="box_selection 2 l2_18D sidebar_l2_18D hover <?php if(get_option_tree('18d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('18d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('18d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 18D</a>
																					
																					<span class="info_box info_l2_18D hide" style="display: none;" > 
																							
																					</span>
																				</li>
																				
																			</ul>
																		</li>						  				
														  				
														  				<li><a href="#" class="box_selection_level 1 level_title_1 level_title down">I Piętro</a>
														  					
														  					<ul class="group_list_side" style="display: none;">
					
														  						
														  						<li><a href="#" class="box_selection 1 l1_7D sidebar_l1_7D hover <?php if(get_option_tree('7d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('7d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('7d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 7D</a>
														  							
														  							<span class="info_box info_l1_7D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 1 l1_8D sidebar_l1_8D hover <?php if(get_option_tree('8d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('8d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('8d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 8D</a>
														  							
														  							<span class="info_box info_l1_8D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 1 l1_9D sidebar_l1_9D hover <?php if(get_option_tree('9d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('9d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('9d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 9D</a>
														  							
														  							<span class="info_box info_l1_9D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 1 l1_10D sidebar_l1_10D hover <?php if(get_option_tree('10d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('10d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('10d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 10D</a>
														  							
														  							<span class="info_box info_l1_10D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 1 l1_11D sidebar_l1_11D hover <?php if(get_option_tree('11d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('11d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('11d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 11D</a>
														  							
														  							<span class="info_box info_l1_11D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 1 l1_12D sidebar_l1_12D hover <?php if(get_option_tree('12d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('12d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('12d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 12D</a>
														  							
														  							<span class="info_box info_l1_12D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						
														  						
														  					</ul>
														  				</li>
														  				
														  				<li><a href="#" class="box_selection_level 0 level_title_0 level_title down">Parter</a>
														  					
														  					<ul class="group_list_side" style="display: none;">
														  						
														  						<li><a href="#" class="box_selection 0 l0_1D sidebar_l0_1D hover <?php if(get_option_tree('1d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('1d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('1d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 1D</a>
														  							
														  							<span class="info_box info_l0_1D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 0 l0_2D sidebar_l0_2D hover <?php if(get_option_tree('2d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('2d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('2d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 2D</a>
														  							
														  							<span class="info_box info_l0_2D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 0 l0_3D sidebar_l0_3D hover <?php if(get_option_tree('3d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('3d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('3d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 3D</a>
														  							
														  							<span class="info_box info_l0_3D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 0 l0_4D sidebar_l0_4D hover <?php if(get_option_tree('4d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('4d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('4d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 4D</a>
														  							
														  							<span class="info_box info_l0_4D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 0 l0_5D sidebar_l0_5D hover <?php if(get_option_tree('5d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('5d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('5d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 5D</a>
														  							
														  							<span class="info_box info_l0_5D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  						
														  						<li><a href="#" class="box_selection 0 l0_6D sidebar_l0_6D hover <?php if(get_option_tree('6d', '', false, true, 0 ) == "Wolne" ) {
												  						echo "available"; } ?><?php if(get_option_tree('6d', '', false, true, 0 ) == "Sprzedane" ) {
												  						echo "sold"; } ?><?php if(get_option_tree('6d', '', false, true, 0 ) == "Rezerwacja" ) {
												  						echo "reserved"; } ?>">Mieszkanie 6D</a>
														  							
														  							<span class="info_box info_l0_6D hide" style="display: none;" > 
														  									
														  							</span>
														  						</li>
														  					</ul>
														  				</li>
														  				
													  		</ul>
										</div>
					
					
					</div>
					<div class="plany_infoSidebar content_left">

						<div id="infoLoader">
							
						</div>
					<span class="loader" style="opacity: 0"></span>
					<span class="legenda">
					<h5>Legenda</h5>
					<span class="legendAvailable">Wolne</span>
					<span class="legendaSold">Sprzedane</span>
					<span class="legendReserved">Rezerwacja</span>
					</span>
					</div>
					
					
					<div class="clear"></div>					
														
				
				</div>
			</article>
			
			<div id="maps" style="display: none;">
								<!--  Map 4  -->
									<map name="map0" id="map0">
									
									<area shape="poly" alt="" coords="136,124, 135,125, 136,124" class="box_selection 3 l3_korytarz1 hover" href="#" />
									<area shape="poly" alt="" coords="135,137, 123,130, 135,137" class="box_selection 3 l3_korytarz1 hover" href="#" />
									<area shape="poly" alt="" coords="250,68, 194,94, 199,97, 211,103, 225,112, 225,130, 256,116, 262,119, 273,114, 273,108, 281,104, 281,86, 250,68" class="box_selection 3 l3_16C hover" href="#" />
									<area shape="poly" alt="" coords="226,35, 222,36, 216,33, 205,38, 205,44, 157,66, 199,91, 207,88, 219,82, 250,68, 254,70, 256,70, 256,52, 226,35" class="box_selection 3 l3_17C hover" href="#" />
									<area shape="poly" alt="" coords="305,24, 297,19, 292,16, 301,12, 312,7, 301,0, 281,9, 274,6, 263,11, 263,17, 269,15, 263,17, 226,35, 256,52, 256,70, 262,73, 262,56, 277,49, 283,53, 285,52, 297,46, 296,46, 296,45, 285,39, 296,34, 292,32, 290,31, 292,30, 305,24" class="box_selection 3 l3_18C hover" href="#" />
									<area shape="poly" alt="" coords="312,7, 292,16, 306,24, 292,30, 290,31, 296,34, 290,36, 285,39, 297,46, 285,52, 283,53, 277,49, 262,56, 262,73, 268,77, 300,96, 315,89, 321,92, 332,87, 332,81, 375,61, 375,44, 312,7" class="box_selection 3 l3_19C hover" href="#" />
									<area shape="poly" alt="" coords="119,128, 109,133, 96,125, 81,132, 94,140, 81,145, 43,163, 43,180, 74,199, 124,176, 130,180, 141,174, 141,141, 119,128" class="box_selection 3 l3_19D hover" href="#" />
									<area shape="poly" alt="" coords="79,102, 32,123, 26,119, 15,125, 15,131, 0,138, 0,155, 43,180, 43,163, 94,140, 81,132, 92,127, 96,125, 109,133, 109,119, 79,102" class="box_selection 3 l3_20D hover" href="#" />
									<area shape="poly" alt="" coords="150,63, 139,68, 139,74, 90,97, 84,93, 73,98, 73,105, 79,102, 79,102, 84,105, 90,108, 96,112, 109,119, 109,133, 119,128, 123,130, 136,124, 135,120, 136,120, 136,107, 148,101, 161,109, 176,102, 164,94, 169,92, 181,99, 194,94, 199,91, 150,63" class="box_selection 3 l3_21D hover" href="#" />
									<area shape="poly" alt="" coords="194,94, 181,99, 169,92, 164,94, 176,102, 161,109, 148,101, 136,107, 136,120, 135,120, 135,137, 141,141, 142,142, 158,151, 174,144, 183,149, 190,152, 201,147, 201,141, 225,130, 225,112, 194,94" class="box_selection 3 l3_22D hover" href="#" />
									<area shape="poly" alt="" coords="74,198, 30,173, 30,190, 74,216, 108,201, 108,183, 74,198" class="box_selection 2 l2_14D hover" href="#" />
									<area shape="poly" alt="" coords="142,185, 151,181, 151,164, 141,158, 142,168, 142,172, 141,174, 130,179, 124,176, 108,183, 108,201, 124,193, 130,197, 141,192, 142,185" class="box_selection 2 l2_13D hover" href="#" />
									<area shape="poly" alt="" coords="0,155, 0,173, 30,190, 30,173, 0,155" class="box_selection 2 l2_15D hover" href="#" />
									<area shape="poly" alt="" coords="201,141, 201,147, 189,152, 183,149, 174,144, 158,151, 142,142, 142,158, 150,163, 151,164, 167,174, 183,166, 190,170, 201,165, 201,158, 225,147, 225,129, 201,141" class="box_selection 2 l2_18D hover" href="#" />
									<area shape="poly" alt="" coords="332,80, 332,87, 321,92, 315,89, 300,95, 268,77, 268,79, 281,86, 281,102, 300,113, 315,106, 321,110, 332,105, 332,98, 332,98, 375,79, 375,61, 332,80" class="box_selection 2 l2_15C hover" href="#" />
									<area shape="poly" alt="" coords="273,108, 273,114, 262,119, 256,116, 225,130, 225,147, 256,133, 262,137, 273,132, 273,125, 281,121, 281,104, 273,108" class="box_selection 2 l2_11C hover" href="#" />
									<area shape="poly" alt="" coords="74,216, 30,190, 30,208, 74,234, 107,218, 107,201, 74,216" class="box_selection 1 l1_8D hover" href="#" />
									<area shape="poly" alt="" coords="142,185, 141,192, 130,197, 124,193, 107,201, 107,218, 124,211, 130,214, 141,209, 141,203, 151,199, 151,181, 142,185" class="box_selection 1 l1_7D hover" href="#" />
									<area shape="poly" alt="" coords="0,173, 30,190, 30,208, 0,190, 0,173" href="9D hover" href="#" />
									<area shape="poly" alt="" coords="201,158, 201,165, 190,170, 184,166, 167,174, 151,164, 151,182, 167,191, 184,184, 190,187, 201,182, 201,176, 225,165, 225,147, 201,158" class="box_selection 1 l1_12D hover" href="#" />
									<area shape="poly" alt="" coords="332,98, 332,104, 321,110, 315,106, 300,113, 268,94, 268,96, 281,104, 281,120, 300,130, 315,124, 321,127, 332,122, 332,116, 375,96, 375,79, 332,98" class="box_selection 1 l1_10C hover" href="#" />
									<area shape="poly" alt="" coords="273,125, 273,131, 262,137, 256,133, 225,147, 225,165, 255,151, 262,154, 273,149, 273,143, 281,139, 281,122, 273,125" class="box_selection 1 l1_6C hover" href="#" />
									<area shape="poly" alt="" coords="250,241, 194,266, 225,285, 225,302, 256,288, 262,292, 262,292, 273,287, 273,280, 273,280, 281,276, 281,259, 250,241" class="box_selection 0 l0_1C hover" href="#" />
									<area shape="poly" alt="" coords="64,313, 31,328, 31,345, 75,371, 108,356, 108,338, 64,313" class="box_selection 0 l0_2D hover" href="#" />
									<area shape="poly" alt="" coords="119,300, 77,320, 108,338, 108,356, 124,348, 130,352, 141,347, 141,340, 151,336, 151,319, 119,300" class="box_selection 0 l0_1D hover" href="#" />
									<area shape="poly" alt="" coords="46,289, 32,296, 26,292, 15,297, 21,301, 15,303, 0,310, 0,328, 30,345, 30,328, 59,314, 63,313, 76,320, 76,307, 46,289" class="box_selection 0 l0_3D hover" href="#" />
									<area shape="poly" alt="" coords="125,285, 95,267, 90,269, 84,265, 73,271, 73,277, 46,289, 77,307, 76,320, 119,300, 124,303, 125,302, 125,285" class="box_selection 0 l0_4D hover" href="#" />
									<area shape="poly" alt="" coords="237,242, 237,233, 207,215, 156,239, 150,235, 139,240, 139,247, 138,247, 168,264, 169,278, 181,272, 194,266, 228,250, 241,244, 237,242" class="box_selection 0 l0_2C hover" href="#" />
									<area shape="poly" alt="" coords="375,216, 343,198, 269,232, 268,249, 283,258, 300,268, 315,261, 315,261, 321,265, 321,265, 332,260, 332,253, 375,234, 375,216" class="box_selection 0 l0_5C hover" href="#" />
									<area shape="poly" alt="" coords="255,194, 222,209, 216,205, 205,210, 205,217, 207,215, 211,218, 216,221, 222,224, 224,225, 237,233, 237,242, 241,244, 250,241, 254,243, 269,236, 269,232, 285,224, 285,211, 255,194" class="box_selection 0 l0_3C hover" href="#" />
									<area shape="poly" alt="" coords="301,173, 281,182, 274,178, 263,183, 269,187, 263,190, 255,194, 269,202, 285,211, 285,224, 297,219, 343,198, 301,173" class="box_selection 0 l0_4C hover" href="#" />
									<area shape="poly" alt="" coords="194,266, 136,293, 136,293, 136,310, 167,329, 167,329, 183,321, 189,325, 189,325, 201,320, 201,313, 225,302, 225,285, 194,266" class="box_selection 0 l0_6D hover" href="#" />
									<area shape="poly" alt="" coords="139,247, 95,267, 112,277, 125,285, 125,302, 136,297, 136,293, 169,278, 169,265, 139,247" class="box_selection 0 l0_5D hover" href="#" />
									
									</map>
							
								<!--  Map 3  -->
									<map name="map1" id="map1">
									
									<area shape="poly" alt="" coords="139,229, 95,249, 125,267, 125,285, 135,280, 135,275, 148,270, 148,256, 169,247, 139,229" class="box_selection 1 l1_11D hover" href="#" />
									<area shape="poly" alt="" coords="63,295, 30,310, 30,328, 74,354, 107,338, 107,321, 63,295" class="box_selection 1 l1_8D hover" href="#" />
									<area shape="poly" alt="" coords="119,283, 76,302, 93,312, 107,321, 108,338, 124,331, 130,334, 141,329, 141,323, 151,319, 151,301, 119,283" class="box_selection 1 l1_7D hover" href="#" />
									<area shape="poly" alt="" coords="46,272, 32,278, 26,274, 15,279, 15,286, 0,293, 0,310, 30,328, 30,310, 63,295, 76,302, 76,289, 46,272" class="box_selection 1 l1_9D hover" href="#" />
									<area shape="poly" alt="" coords="95,249, 90,251, 84,248, 73,253, 73,253, 73,259, 73,259, 46,271, 76,289, 76,302, 119,283, 123,285, 125,285, 125,267, 95,249" class="box_selection 1 l1_10D hover" href="#" />
									<area shape="poly" alt="" coords="194,249, 181,254, 169,247, 148,256, 148,270, 136,275, 135,292, 150,301, 150,302, 167,311, 183,304, 190,307, 201,302, 201,296, 225,285, 225,267, 194,249" class="box_selection 1 l1_12D hover" href="#" />
									<area shape="poly" alt="" coords="343,180, 269,214, 268,232, 300,250, 315,244, 321,247, 332,242, 332,236, 375,216, 375,199, 343,180" class="box_selection 1 l1_10C hover" href="#" />
									<area shape="poly" alt="" coords="250,223, 194,249, 225,267, 225,285, 255,271, 261,274, 272,269, 272,263, 281,259, 281,242, 281,241, 250,223" class="box_selection 1 l1_6C hover" href="#" />
									<area shape="poly" alt="" coords="237,225, 237,215, 207,198, 156,221, 150,217, 139,223, 145,226, 139,229, 139,229, 145,233, 149,235, 169,247, 181,254, 194,249, 229,233, 241,227, 237,225" class="box_selection 1 l1_7C hover" href="#" />
									<area shape="poly" alt="" coords="255,176, 222,191, 216,187, 205,193, 205,199, 207,198, 211,200, 216,203, 222,207, 224,208, 237,215, 237,225, 241,227, 250,223, 254,225, 268,219, 268,214, 285,207, 285,194, 255,176" class="box_selection 1 l1_8C hover" href="#" />
									<area shape="poly" alt="" coords="301,155, 281,164, 274,161, 263,166, 263,172, 255,176, 285,194, 285,207, 297,201, 343,180, 301,155" class="box_selection 1 l1_9C hover" href="#" />
									<area shape="poly" alt="" coords="136,124, 135,125, 136,124" class="box_selection 3 l3_korytarz1 hover" href="#" />
									<area shape="poly" alt="" coords="135,137, 123,130, 135,137" class="box_selection 3 l3_korytarz1 hover" href="#" />
									<area shape="poly" alt="" coords="250,68, 194,94, 199,97, 211,103, 225,112, 225,130, 256,116, 262,119, 273,114, 273,108, 281,104, 281,86, 250,68" class="box_selection 3 l3_16C hover" href="#" />
									<area shape="poly" alt="" coords="226,35, 222,36, 216,33, 205,38, 205,44, 157,66, 199,91, 207,88, 219,82, 250,68, 254,70, 256,70, 256,52, 226,35" class="box_selection 3 l3_17C hover" href="#" />
									<area shape="poly" alt="" coords="305,24, 297,19, 292,16, 301,12, 312,7, 301,0, 281,9, 274,6, 263,11, 263,17, 269,15, 263,17, 226,35, 256,52, 256,70, 262,73, 262,56, 277,49, 283,53, 285,52, 297,46, 296,46, 296,45, 285,39, 296,34, 292,32, 290,31, 292,30, 305,24" class="box_selection 3 l3_18C hover" href="#" />
									<area shape="poly" alt="" coords="312,7, 292,16, 306,24, 292,30, 290,31, 296,34, 290,36, 285,39, 297,46, 285,52, 283,53, 277,49, 262,56, 262,73, 268,77, 300,96, 315,89, 321,92, 332,87, 332,81, 375,61, 375,44, 312,7" class="box_selection 3 l3_19C hover" href="#" />
									<area shape="poly" alt="" coords="119,128, 109,133, 96,125, 81,132, 94,140, 81,145, 43,163, 43,180, 74,199, 124,176, 130,180, 141,174, 141,141, 119,128" class="box_selection 3 l3_19D hover" href="#" />
									<area shape="poly" alt="" coords="79,102, 32,123, 26,119, 15,125, 15,131, 0,138, 0,155, 43,180, 43,163, 94,140, 81,132, 92,127, 96,125, 109,133, 109,119, 79,102" class="box_selection 3 l3_20D hover" href="#" />
									<area shape="poly" alt="" coords="150,63, 139,68, 139,74, 90,97, 84,93, 73,98, 73,105, 79,102, 79,102, 84,105, 90,108, 96,112, 109,119, 109,133, 119,128, 123,130, 136,124, 135,120, 136,120, 136,107, 148,101, 161,109, 176,102, 164,94, 169,92, 181,99, 194,94, 199,91, 150,63" class="box_selection 3 l3_21D hover" href="#" />
									<area shape="poly" alt="" coords="194,94, 181,99, 169,92, 164,94, 176,102, 161,109, 148,101, 136,107, 136,120, 135,120, 135,137, 141,141, 142,142, 158,151, 174,144, 183,149, 190,152, 201,147, 201,141, 225,130, 225,112, 194,94" class="box_selection 3 l3_22D hover" href="#" />
									<area shape="poly" alt="" coords="74,198, 30,173, 30,190, 74,216, 108,201, 108,183, 74,198" class="box_selection 2 l2_14D hover" href="#" />
									<area shape="poly" alt="" coords="142,185, 151,181, 151,164, 141,158, 142,168, 142,172, 141,174, 130,179, 124,176, 108,183, 108,201, 124,193, 130,197, 141,192, 142,185" class="box_selection 2 l2_13D hover" href="#" />
									<area shape="poly" alt="" coords="0,155, 0,173, 30,190, 30,173, 0,155" class="box_selection 2 l2_15D hover" href="#" />
									<area shape="poly" alt="" coords="201,141, 201,147, 189,152, 183,149, 174,144, 158,151, 142,142, 142,158, 150,163, 151,164, 167,174, 183,166, 190,170, 201,165, 201,158, 225,147, 225,129, 201,141" class="box_selection 2 l2_18D hover" href="#" />
									<area shape="poly" alt="" coords="332,80, 332,87, 321,92, 315,89, 300,95, 268,77, 268,79, 281,86, 281,102, 300,113, 315,106, 321,110, 332,105, 332,98, 332,98, 375,79, 375,61, 332,80" class="box_selection 2 l2_15C hover" href="#" />
									<area shape="poly" alt="" coords="273,108, 273,114, 262,119, 256,116, 225,130, 225,147, 256,133, 262,137, 273,132, 273,125, 281,121, 281,104, 273,108" class="box_selection 2 l2_11C hover" href="#" />
									<area shape="poly" alt="" coords="281,257, 300,268, 281,276, 281,257" class="box_selection 0 l0_korytarz2 hover" href="#" />
									<area shape="poly" alt="" coords="273,263, 273,269, 262,274, 256,271, 225,285, 225,302, 256,288, 262,292, 273,287, 273,280, 281,276, 281,259, 273,263" class="box_selection 0 l0_1C hover" href="#" />
									<area shape="poly" alt="" coords="75,354, 30,328, 30,345, 75,371, 108,356, 108,338, 75,354" class="box_selection 0 l0_2D hover" href="#" />
									<area shape="poly" alt="" coords="141,323, 142,329, 130,334, 124,331, 108,338, 108,356, 124,348, 130,352, 141,347, 141,340, 151,336, 151,319, 141,323" class="box_selection 0 l0_1D hover" href="#" />
									<area shape="poly" alt="" coords="0,310, 0,328, 30,345, 30,328, 0,310" class="box_selection 0 l0_3D hover" href="#" />
									<area shape="poly" alt="" coords="281,240, 281,257, 283,258, 300,268, 315,261, 321,265, 332,260, 332,253, 375,234, 375,216, 332,236, 332,242, 321,247, 315,244, 300,250, 281,240" class="box_selection 0 l0_5C hover" href="#" />
									<area shape="poly" alt="" coords="200,296, 201,302, 189,307, 183,304, 167,311, 151,302, 151,320, 167,329, 183,321, 190,325, 201,320, 201,313, 225,302, 225,285, 200,296" class="box_selection 0 l0_6D hover" href="#" />
									
									</map>
									
								<!--  Map 2  -->
									<map name="map2" id="map2">
									
									<area shape="poly" alt="" coords="136,124, 135,125, 135,137, 123,130, 136,124" class="box_selection 3 l3_korytarz1 hover" href="#" />
									<area shape="poly" alt="" coords="281,104, 300,96, 256,70, 254,70, 281,86, 281,104" class="box_selection 3 l3_korytarz2 hover" href="#" />
									<area shape="poly" alt="" coords="250,68, 194,94, 199,97, 211,103, 225,112, 225,130, 256,116, 262,119, 273,114, 273,108, 281,104, 281,86, 250,68" class="box_selection 3 l3_16C hover" href="#" />
									<area shape="poly" alt="" coords="226,35, 222,36, 216,33, 205,38, 205,44, 157,66, 199,91, 207,88, 219,82, 250,68, 254,70, 256,70, 256,52, 226,35" class="box_selection 3 l3_17C hover" href="#" />
									<area shape="poly" alt="" coords="305,24, 297,19, 292,16, 301,12, 312,7, 301,0, 281,9, 274,6, 263,11, 263,17, 269,15, 263,17, 226,35, 256,52, 256,70, 262,73, 262,56, 277,49, 283,53, 285,52, 297,46, 296,46, 296,45, 285,39, 296,34, 292,32, 290,31, 292,30, 305,24" class="box_selection 3 l3_18C hover" href="#" />
									<area shape="poly" alt="" coords="312,7, 292,16, 306,24, 292,30, 290,31, 296,34, 290,36, 285,39, 297,46, 285,52, 283,53, 277,49, 262,56, 262,73, 268,77, 300,96, 315,89, 321,92, 332,87, 332,81, 375,61, 375,44, 312,7" class="box_selection 3 l3_19C hover" href="#" />
									<area shape="poly" alt="" coords="119,128, 109,133, 96,125, 81,132, 94,140, 81,145, 43,163, 43,180, 74,199, 124,176, 130,180, 141,174, 141,141, 119,128" class="box_selection 3 l3_19D hover" href="#" />
									<area shape="poly" alt="" coords="79,102, 32,123, 26,119, 15,125, 15,131, 0,138, 0,155, 43,180, 43,163, 94,140, 81,132, 92,127, 96,125, 109,133, 109,119, 79,102" class="box_selection 3 l3_20D hover" href="#" />
									<area shape="poly" alt="" coords="150,63, 139,68, 139,74, 90,97, 84,93, 73,98, 73,105, 79,102, 79,102, 84,105, 90,108, 96,112, 109,119, 109,133, 119,128, 123,130, 136,124, 135,120, 136,120, 136,107, 148,101, 161,109, 176,102, 164,94, 169,92, 181,99, 194,94, 199,91, 150,63" class="box_selection 3 l3_21D hover" href="#" />
									<area shape="poly" alt="" coords="194,94, 181,99, 169,92, 164,94, 176,102, 161,109, 148,101, 136,107, 136,120, 135,120, 135,137, 141,141, 142,142, 158,151, 174,144, 183,149, 190,152, 201,147, 201,141, 225,130, 225,112, 194,94" class="box_selection 3 l3_22D hover" href="#" />
									<area shape="poly" alt="" coords="64,277, 30,293, 30,310, 74,336, 108,321, 108,303, 64,277" class="box_selection 2 l2_14D hover" href="#" />
									<area shape="poly" alt="" coords="119,265, 76,285, 93,295, 108,303, 108,321, 124,313, 130,317, 130,317, 141,312, 141,305, 151,301, 150,284, 119,265" class="box_selection 2 l2_13D hover" href="#" />
									<area shape="poly" alt="" coords="46,254, 32,260, 26,257, 15,262, 15,268, 15,268, 0,275, 0,293, 30,310, 30,293, 63,278, 76,285, 76,272, 46,254" class="box_selection 2 l2_15D hover" href="#" />
									<area shape="poly" alt="" coords="95,232, 90,234, 84,230, 73,235, 73,242, 46,254, 76,272, 76,285, 119,265, 123,268, 125,267, 125,250, 95,232" class="box_selection 2 l2_16D hover" href="#" />
									<area shape="poly" alt="" coords="183,286, 189,290, 201,285, 201,278, 183,286" class="box_selection 2 l2_18D hover" href="#" />
									<area shape="poly" alt="" coords="225,249, 194,231, 181,237, 169,229, 148,239, 148,252, 136,258, 136,275, 151,284, 151,284, 167,293, 225,267, 225,249" class="box_selection 2 l2_18D hover" href="#" />
									<area shape="poly" alt="" coords="139,212, 95,232, 125,249, 125,267, 136,262, 136,258, 148,252, 148,239, 169,229, 139,212" class="box_selection 2 l2_17D hover" href="#" />
									<area shape="poly" alt="" coords="343,163, 268,197, 268,214, 300,233, 315,226, 321,230, 332,225, 332,218, 375,199, 375,181, 343,163" class="box_selection 2 l2_15C hover" href="#" />
									<area shape="poly" alt="" coords="281,224, 250,205, 194,231, 225,249, 225,267, 256,253, 262,256, 273,251, 273,245, 281,241, 281,224" class="box_selection 2 l2_11C hover" href="#" />
									<area shape="poly" alt="" coords="237,207, 237,198, 207,180, 156,204, 150,200, 139,205, 139,212, 139,212, 145,215, 149,218, 169,229, 181,237, 194,231, 229,215, 241,209, 237,207" class="box_selection 2 l2_12C hover" href="#" />
									<area shape="poly" alt="" coords="255,159, 222,174, 216,170, 205,175, 205,182, 207,180, 211,182, 216,185, 222,189, 224,190, 237,198, 237,207, 241,209, 250,205, 254,208, 268,201, 269,197, 285,189, 285,176, 255,159" class="box_selection 2 l2_13C hover" href="#" />
									<area shape="poly" alt="" coords="301,138, 281,147, 274,143, 263,148, 263,155, 255,159, 285,176, 285,189, 297,184, 343,163, 301,138" class="box_selection 2 l2_14C hover" href="#" />
									<area shape="poly" alt="" coords="74,336, 30,310, 30,328, 74,354, 107,338, 107,321, 74,336" class="box_selection 1 l1_8D hover" href="#" />
									<area shape="poly" alt="" coords="142,305, 141,312, 130,317, 124,313, 107,321, 107,338, 124,331, 130,334, 141,329, 141,323, 151,319, 151,301, 142,305" class="box_selection 1 l1_7D hover" href="#" />
									<area shape="poly" alt="" coords="0,293, 30,310, 30,328, 0,310, 0,293" href="9D hover" href="#" />
									<area shape="poly" alt="" coords="201,278, 201,285, 190,290, 184,286, 167,294, 151,284, 151,302, 167,311, 184,304, 190,307, 201,302, 201,296, 225,285, 225,267, 201,278" class="box_selection 1 l1_12D hover" href="#" />
									<area shape="poly" alt="" coords="332,218, 332,224, 321,230, 315,226, 300,233, 268,214, 268,216, 281,224, 281,240, 300,250, 315,244, 321,247, 332,242, 332,236, 375,216, 375,199, 332,218" class="box_selection 1 l1_10C hover" href="#" />
									<area shape="poly" alt="" coords="273,245, 273,251, 262,257, 256,253, 225,267, 225,285, 255,271, 262,274, 273,269, 273,263, 281,259, 281,242, 273,245" class="box_selection 1 l1_6C hover" href="#" />
									<area shape="poly" alt="" coords="281,257, 300,268, 281,276, 281,257" class="box_selection 0 l0_korytarz2 hover" href="#" />
									<area shape="poly" alt="" coords="273,263, 273,269, 262,274, 256,271, 225,285, 225,302, 256,288, 262,292, 273,287, 273,280, 281,276, 281,259, 273,263" class="box_selection 0 l0_1C hover" href="#" />
									<area shape="poly" alt="" coords="75,354, 30,328, 30,345, 75,371, 108,356, 108,338, 75,354" class="box_selection 0 l0_2D hover" href="#" />
									<area shape="poly" alt="" coords="141,323, 142,329, 130,334, 124,331, 108,338, 108,356, 124,348, 130,352, 141,347, 141,340, 151,336, 151,319, 141,323" class="box_selection 0 l0_1D hover" href="#" />
									<area shape="poly" alt="" coords="0,310, 0,328, 30,345, 30,328, 0,310" class="box_selection 0 l0_3D hover" href="#" />
									<area shape="poly" alt="" coords="281,240, 281,257, 283,258, 300,268, 315,261, 321,265, 332,260, 332,253, 375,234, 375,216, 332,236, 332,242, 321,247, 315,244, 300,250, 281,240" class="box_selection 0 l0_5C hover" href="#" />
									<area shape="poly" alt="" coords="200,296, 201,302, 189,307, 183,304, 167,311, 151,302, 151,320, 167,329, 183,321, 190,325, 201,320, 201,313, 225,302, 225,285, 200,296" class="box_selection 0 l0_6D hover" href="#" />
									
									</map>
									
								<!--  Map 1  -->
									<map name="map3" id="map3">
									<area shape="poly" alt="" coords="136,244, 135,245, 135,257, 123,250, 136,244" class="box_selection 3 L3_korytarz1 hover" href="#">
									<area shape="poly" alt="" coords="250,188, 194,214, 199,217, 211,223, 225,232, 225,250, 256,236, 262,239, 273,234, 273,228, 281,224, 281,206, 250,188" class="box_selection 3 l3_16C hover" href="#">
									<area shape="poly" alt="" coords="226,155, 222,156, 216,153, 205,158, 205,164, 157,186, 199,211, 207,208, 219,202, 250,188, 254,190, 256,190, 256,172, 226,155" class="box_selection 3 l3_17C hover" href="#">
									<area shape="poly" alt="" coords="305,144, 297,139, 292,136, 301,132, 312,127, 301,120, 281,129, 274,126, 263,131, 263,137, 269,135, 263,137, 226,155, 256,172, 256,190, 262,193, 262,176, 277,169, 283,173, 285,172, 297,166, 296,166, 296,165, 285,159, 296,154, 292,152, 290,151, 292,150, 305,144" class="box_selection 3 l3_18C hover" href="#">
									<area shape="poly" alt="" coords="312,127, 292,136, 306,144, 292,150, 290,151, 296,154, 290,156, 285,159, 297,166, 285,172, 283,173, 277,169, 262,176, 262,193, 268,197, 300,216, 315,209, 321,212, 332,207, 332,201, 375,181, 375,164, 312,127" class="box_selection 3 l3_19C hover" href="#">
									<area shape="poly" alt="" coords="119,248, 109,253, 96,245, 81,252, 94,260, 81,265, 43,283, 43,300, 74,319, 124,296, 130,300, 141,294, 141,261, 119,248" class="box_selection 3 l3_19D hover" href="#">
									<area shape="poly" alt="" coords="79,222, 32,243, 26,239, 15,245, 15,251, 0,258, 0,275, 43,300, 43,283, 94,260, 81,252, 92,247, 96,245, 109,253, 109,239, 79,222" class="box_selection 3 l3_20D hover" href="#">
									<area shape="poly" alt="" coords="150,183, 139,188, 139,194, 90,217, 84,213, 73,218, 73,225, 79,222, 79,222, 84,225, 90,228, 96,232, 109,239, 109,253, 119,248, 123,250, 136,244, 135,240, 136,240, 136,227, 148,221, 161,229, 176,222, 164,214, 169,212, 181,219, 194,214, 199,211, 150,183" class="box_selection 3 l3_21D hover" href="#">
									<area shape="poly" alt="" coords="194,214, 181,219, 169,212, 164,214, 176,222, 161,229, 148,221, 136,227, 136,240, 135,240, 135,257, 141,261, 142,262, 158,271, 174,264, 183,269, 190,272, 201,267, 201,261, 225,250, 225,232, 194,214" class="box_selection 3 l3_22D hover" href="#">
									<area shape="poly" alt="" coords="74,318, 30,293, 30,310, 74,336, 108,321, 108,303, 74,318" class="box_selection 2 l2_14D hover" href="#">
									<area shape="poly" alt="" coords="142,305, 151,301, 151,284, 141,278, 142,288, 142,292, 141,294, 130,299, 124,296, 108,303, 108,321, 124,313, 130,317, 141,312, 142,305" class="box_selection 2 l2_13D hover" href="#">
									<area shape="poly" alt="" coords="0,275, 0,293, 30,310, 30,293, 0,275" class="box_selection 2 l2_15D hover" href="#">
									<area shape="poly" alt="" coords="201,261, 201,267, 189,272, 183,269, 174,264, 158,271, 142,262, 142,278, 150,283, 151,284, 167,294, 183,286, 190,290, 201,285, 201,278, 225,267, 225,249, 201,261" class="box_selection 2 l2_18D hover" href="#">
									<area shape="poly" alt="" coords="332,200, 332,207, 321,212, 315,209, 300,215, 268,197, 268,199, 281,206, 281,222, 300,233, 315,226, 321,230, 332,225, 332,218, 332,218, 375,199, 375,181, 332,200" class="box_selection 2 l2_15C hover" href="#">
									<area shape="poly" alt="" coords="274,228,274,234,263,239,257,236,226,250,226,267,257,253,263,257,274,252,274,245,282,241,282,224,274,228" class="box_selection 2 l2_11C hover" href="#">
									<area shape="poly" alt="" coords="74,336, 30,310, 30,328, 74,354, 107,338, 107,321, 74,336" class="box_selection 1 l1_8D hover" href="#">
									<area shape="poly" alt="" coords="142,305, 141,312, 130,317, 124,313, 107,321, 107,338, 124,331, 130,334, 141,329, 141,323, 151,319, 151,301, 142,305" class="box_selection 1 l1_7D hover" href="#">
									<area shape="poly" alt="" coords="0,293, 30,310, 30,328, 0,310, 0,293" class="box_selection 1 l1_9D hover" href="#">
									<area shape="poly" alt="" coords="201,278, 201,285, 190,290, 184,286, 167,294, 151,284, 151,302, 167,311, 184,304, 190,307, 201,302, 201,296, 225,285, 225,267, 201,278" class="box_selection 1 l1_12D hover" href="#">
									<area shape="poly" alt="" coords="332,218, 332,224, 321,230, 315,226, 300,233, 268,214, 268,216, 281,224, 281,240, 300,250, 315,244, 321,247, 332,242, 332,236, 375,216, 375,199, 332,218" class="box_selection 1 l1_10C hover" href="#">
									<area shape="poly" alt="" coords="273,245, 273,251, 262,257, 256,253, 225,267, 225,285, 255,271, 262,274, 273,269, 273,263, 281,259, 281,242, 273,245" class="box_selection 1 l1_6C hover" href="#">
									<area shape="poly" alt="" coords="281,257, 300,268, 281,276, 281,257" class="box_selection 0 l0_korytarz2 hover" href="#">
									<area shape="poly" alt="" coords="273,263, 273,269, 262,274, 256,271, 225,285, 225,302, 256,288, 262,292, 273,287, 273,280, 281,276, 281,259, 273,263" class="box_selection 0 l0_1C hover" href="#">
									<area shape="poly" alt="" coords="75,354, 30,328, 30,345, 75,371, 108,356, 108,338, 75,354" class="box_selection 0 l0_2D hover" href="#""l0_2D">
									<area shape="poly" alt="" coords="141,323, 142,329, 130,334, 124,331, 108,338, 108,356, 124,348, 130,352, 141,347, 141,340, 151,336, 151,319, 141,323" class="box_selection 0 l0_1D hover" href="#""l0_1D">
									<area shape="poly" alt="" coords="0,310, 0,328, 30,345, 30,328, 0,310" class="box_selection 0 l0_3D hover" href="#">
									<area shape="poly" alt="" coords="281,240, 281,257, 283,258, 300,268, 315,261, 321,265, 332,260, 332,253, 375,234, 375,216, 332,236, 332,242, 321,247, 315,244, 300,250, 281,240" class="box_selection 0 l0_5C hover" href="#">
									<area shape="poly" alt="" coords="200,296, 201,302, 189,307, 183,304, 167,311, 151,302, 151,320, 167,329, 183,321, 190,325, 201,320, 201,313, 225,302, 225,285, 200,296" class="box_selection 0 l0_6D hover" href="#">
									
									</map>
									
								
								
						</div>
						
						
									
			
				
					  </body>
					</html>
			
		</div><!-- #main -->
		
<?php get_footer();?>
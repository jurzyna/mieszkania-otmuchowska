<?php
/*
Template Name: Lokalizacja
*/
?>

<?php 
if( of_get_option('security_question') ):
$securityQuestion = of_get_option('security_question');
else:
$securityQuestion = "What is 2+3?";
endif;

if( of_get_option('security_answer') ):
$securityAnswer = of_get_option('security_answer');
else:
$securityAnswer = "5";
endif;

if( of_get_option('contact_email') ):
$contactEmail = of_get_option('contact_email');
else:
$contactEmail = get_option('admin_email');
endif;

if( of_get_option('contact_subject') ):
$contactSubject = of_get_option('contact_subject');
else:
$contactSubject = 'ShowyCase';
endif;

if(isset($_POST['submitted'])):
	
	// NAME CHECHING
	if(trim($_POST['contactName']) === '') {
	$nameError = __('Wpisz swoje imię.', 'premitheme');
	$hasError = true;
	} else {
	$name = trim($_POST['contactName']);
	}
	
	// EMAIL CHECHING
	if(trim($_POST['email']) === '')  {
	$emailError = __('Wpisz e-mail.', 'premitheme');
	$hasError = true;
	} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+.[A-Z]{2,4}$", trim($_POST['email']))) {
	$emailError = __('Wpisz prawidłowy email.', 'premitheme');
	$hasError = true;
	} else {
	$email = trim($_POST['email']);
	}
	
	// MESSAGE CHECHING
	if(trim($_POST['comments']) === '') {
	$commentError = __('Wpisz treść.', 'premitheme');
	$hasError = true;
	} else {
  	if(function_exists('stripslashes')) {
  	$comments = stripslashes(trim($_POST['comments']));
  	} else {
  	$comments = trim($_POST['comments']);
  	}
	}
	
	// SECURITY CHECHING
	if( of_get_option("use_security") == '' || of_get_option("use_security") != '0' ):
  	if(trim($_POST['security']) === '')  {
  	$securityError = __('You forgot to enter the security answer.', 'premitheme');
  	$hasError = true;
  	} else if (trim($_POST['security']) != $securityAnswer) {
  	$securityError = __('You entered a wrong security answer.', 'premitheme');
  	$hasError = true;
  	}
	endif;
	
	// IF EVERYTHING IS OK
	if(!isset($hasError)):
		
		$emailTo = $contactEmail;
		$subject = '['.$contactSubject.'] from '.$name;
		$body = "Name: $name \n\nEmail: $email \n\nMessage: $comments";
		$headers = 'From: '.$name.' <'.$email.'>' . "\r\n" . 'Reply-To: ' . $email;
		
		mail($emailTo, $subject, $body, $headers);
		
		$emailSent = true;
		
	endif;
endif;
?>

<?php get_header();?>
	
	<section id="content-wrap">
		<div id="main" class="full-lokalizacja">
			
			<?php the_post(); ?>
			
			<article id="post-<?php the_ID();?>" <?php post_class('entry-wrap');?>>
				<div id="contact-map2"><iframe width="726" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.pl/maps?hl=pl&amp;ie=UTF8&amp;q=Mieszkania+Otmuchowska+nysa&amp;fb=1&amp;gl=pl&amp;hq=Mieszkania+Otmuchowska&amp;hnear=Nysa&amp;view=map&amp;cid=10315408786865668549&amp;t=h&amp;ll=50.488422,17.303638&amp;spn=0.054608,0.124454&amp;z=13&amp;iwloc=A&amp;output=embed"></iframe>
				</div>
				
				<?php // SHOW GOOGLE MAP IF SET
				if( of_get_option('google_map') || ( of_get_option('google_lat') && of_get_option('google_lng') ) ): ?>
				
				
				<?php // ELSE SHOW FEATURED IMAGE IF SET
				elseif ( has_post_thumbnail()): ?>
				<div class="entry-thumb">
					<?php the_post_thumbnail('fullwidth-page-image'); ?>
				</div>
				<?php endif; ?>
				
				
								
				
				<h1 class="entry-title"><?php the_title(); ?></h1>
				
				
				<?php // SHOW CONTENT IF NOT EMPTY
				if(trim($post->post_content) != '' ): ?>
				<div class="entry-content">
					<?php the_content(); ?>
					
					<div class="footer-entry-meta">
					<?php edit_post_link( __( 'Edit', 'premitheme'), '<span class="edit-link">', '</span>' ); ?>
					</div>
				</div>
				<?php endif; ?>
				
				
				<?php // CONTACT FORM
				if(isset($emailSent) && $emailSent == true): ?>
								
				<h2 class="thanks"><?php _e('Thanks, your email was successfully sent', 'premitheme') ?></h2>
				
				<?php else: ?>
				
				<?php if(isset($hasError)): ?>
			    <h2 class="error"><?php _e('Pola są nieprawidłowe', 'premitheme') ?></h2>
				<?php endif; ?>
				
								<?php endif; ?>
				<div class="clear"></div>
			</article>
			
			<div class="clear"></div>
			
		</div><!-- #main -->
		
<?php get_footer();?>
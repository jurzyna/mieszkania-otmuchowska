
<?php get_header();?>
	
	<section id="content-wrap">
			<div id="main">
			<article id="portfolio_grid" class="post-139 page type-page status-publish hentry entry-wrap three-col">
				
				<div id="portfolio-header">
									
									<h1 class="entry-title"><?php the_title(); ?></h1>
	
				</div>
		<?php
		 
		/*
		*  View array data (for debugging)
		*/
		 
		 
		/*
		*  Create the Markup for a slider
		*  This example will create the Markup for Flexslider (http://www.woothemes.com/flexslider/)
		*/
		$crop = of_get_option('crop_location');
		$prevHeight = get_post_meta($post->ID, 'prevHeight', TRUE);
		
		$images = get_field('galeria');
		 
		if( $images ): ?>
		
		<div id="previews-wrap">

							
							<ul id="folio-items">
		        <?php foreach( $images as $image ): ?>
		           <li class="digital-painting all folio-wrap folio-thumb"><a class="folio-overlay" title="<?php echo $image['caption']; ?>" href="<?php echo $image['url']; ?>" rel="prettyPhoto[group]"><img src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=<?php echo $image['url']; ?>&amp;h=240&amp;w=240&amp;zc=1&amp;q=100&amp;a=c" alt=""/>
		           							
		           							<div class="folio-title" style="opacity: 0; ">
		           									<?php echo $image['caption']; ?>
		           							</div>
		           							<span class="more-hover"></span>
		           							</a>
		           							
		           </li>
		        <?php endforeach; ?>
		    </ul>
		    
		</div>
		
		  		<?php endif; 
		 
		?></div><!-- #main -->
		
<?php get_footer();?>
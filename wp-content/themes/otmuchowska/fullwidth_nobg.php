<?php
/*
Template Name: Full-width Page No NG
*/
?>
<?php get_header();?>
	
	<section id="content-wrap">
		<div id="main">
			
			<?php the_post(); ?>
			
			<article id="post-<?php the_ID();?>" <?php post_class('entry-wrap');?>>

				
				
					<?php the_content(); ?>
					

			</article>
			
		</div><!-- #main -->
		
<?php get_footer();?>
<?php
global $post;
$tmp_post = $post;
$myposts = get_posts('numberposts=9&order=DESC&orderby=post_date');
if( !empty($myposts) ): ?>

<ul class="recent-posts jcarousel-skin-posts">
	<?php foreach( $myposts as $post ) : setup_postdata($post); ?>
	
	<li>
		<div <?php post_class('recent-post-wrap');?>>
			<?php if ( of_get_option('recent_thumbs') == "1" ): ?>
			<a class="blog-overlay" href="<?php the_permalink(); ?>">
  			<?php
  			if ( has_post_thumbnail()):
  			the_post_thumbnail('folio-3-col');
  			else:?>
  			<img src="<?php echo get_template_directory_uri();?>/images/no-image-thumb.png" alt="No Image"/>
  			<?php endif;?>
  			<span class="more-hover"></span>
      </a>
      <?php endif; ?>
      
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			
			<div class="recent-post-content">
				<?php the_excerpt(); ?>
			</div>
			
			<div class="recent-post-meta">
				<a class="recent-post-date" href="<?php echo get_permalink(); ?>" title="<?php echo get_the_time();; ?>"><?php echo get_the_date(); ?></a>
			</div>
			
			<div class="recent-post-format">
				<div class="format-icon"></div>
			</div>
		</div>
	</li>
	
	<?php endforeach; ?>
	<?php $post = $tmp_post;
	wp_reset_query(); ?>
</ul><!-- .recent-posts -->
<div class="home-block-title">
	<span></span>
	<h2><?php if( of_get_option('recent_posts_label') ) echo of_get_option('recent_posts_label'); ?></h2>
</div>

<?php endif; ?>
<?php

$author = get_the_author();
$author_url = get_author_posts_url( get_the_author_meta( 'ID' ) );
$titleAtt = sprintf( __('View all posts by %s', 'premitheme'), $author );
$videoEmbed = get_post_meta($post->ID, 'videoEmbed', TRUE);
$videoURL = get_post_meta($post->ID, 'videoURL', TRUE);
$m4vPath = get_post_meta($post->ID, 'm4vPath', TRUE);
$ogvPath = get_post_meta($post->ID, 'ogvPath', TRUE);
$videoPoster = get_post_meta($post->ID, 'videoPoster', TRUE);
$videoHeight = get_post_meta($post->ID, 'videoHeight', TRUE);

?>
<article id="post-<?php the_ID();?>" <?php post_class('entry-wrap');?>>
	<?php if ( $videoEmbed ):
    
    $embedDecode = htmlspecialchars_decode($videoEmbed) ?>
    
    <div class="entry-thumb">
      <?php echo stripslashes( $embedDecode ); ?>
    </div>
  
  <?php elseif ( $videoURL ):
  
    $embed_code = wp_oembed_get($videoURL, array('width'=>473)); ?>
  
    <div class="entry-thumb">
      <?php echo $embed_code; ?>
    </div>
  
  <?php elseif ( $ogvPath && $m4vPath ): ?>
  
    <script type="text/javascript">
      jQuery(document).ready(function(){
        jQuery("#jquery-jplayer-<?php the_ID(); ?>").jPlayer({
          ready: function () {
            jQuery(this).jPlayer("setMedia", {
              m4v: "<?php echo $m4vPath; ?>",
              ogv: "<?php echo $ogvPath; ?>",
              poster: "<?php echo $videoPoster; ?>"
            });
          },
          swfPath: "<?php echo PT_JS; ?>",
          supplied: "m4v, ogv",
          cssSelectorAncestor: "#jp-gui-<?php the_ID(); ?>",
          size: {
            width: "473px",
            height: "<?php echo $videoHeight; ?>px"
          }
        })
        .bind(jQuery.jPlayer.event.play, function() { // Bind an event handler to the instance's play event.
          jQuery(this).jPlayer("pauseOthers"); // pause all players except this one.
        });
      });
    </script>
  
    <div class="entry-thumb">
      <div class="jp-video-wrapper">
        <div class="jp-type-single">
          <div id="jquery-jplayer-<?php the_ID(); ?>" class="jp-jplayer"></div>
          <div id="jp-gui-<?php the_ID(); ?>" class="jp-gui">
            <div class="jp-video-play"></div>
            <div class="jp-interface">
              <ul class="jp-controls">
                <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
              </ul>
              <div class="jp-progress">
                <div class="jp-seek-bar">
                  <div class="jp-play-bar"></div>
                </div>
              </div>
              <div class="jp-current-time"></div>
              <div class="jp-duration"></div>
              <div class="jp-volume-bar">
                  <div class="jp-volume-bar-value"></div>
              </div>
            </div><!-- .jp-interface -->
          </div><!-- .jp-gui -->
        </div><!-- .jp-type-single -->
      </div><!-- .jp-video-wrapper -->
    </div><!-- .entry-media -->
  
  <?php endif; ?>
	
	<?php if( is_sticky() ): ?>
	<div class="sticky-badge" title="Sticky Post"></div>
	<?php endif; ?>
	
	<?php if( is_single() ): ?>
	<h1 class="entry-title"><?php the_title(); ?></h1>
	<?php else: ?>
	<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	<?php endif; ?>
	
	<div class="header-entry-meta">
		<span><span>&middot;</span><a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_time();; ?>"><?php echo get_the_date(); ?></a></span>
		
		<span><span>&middot;</span><?php echo get_the_category_list( __(', ', 'premitheme') );?></span>
		
		<?php if( is_single() ): ?>
		<span><span>&middot;</span><?php printf( __('By %s', 'premitheme'), '<a href="'. $author_url .'" title="'. $titleAtt .'">'. $author .'</a>' );?></span>
		<?php edit_post_link( __( 'Edit', 'premitheme'), '<span class="edit-link"><span>&middot;</span>', '</span>' ); ?>
		<?php endif; ?>
	</div>
	
	<div class="entry-content">
		<?php if( is_single() ): ?>
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<p><span>' . __( 'Pages:', 'premitheme' ) . '</span>', 'after' => '</p>' ) ); ?>
		
		<?php if( of_get_option('sharing_on') == '' || of_get_option('sharing_on') != '0' ): ?>
		<?php get_template_part('inc/sharing_btns'); ?>
		<?php endif; ?>
		
		<?php else: ?>
		<?php the_excerpt(); ?>
		<?php endif; ?>
	</div>
	
	<div class="footer-entry-meta">
		<?php echo get_the_tag_list( '<span class="tags-list">', '', '</span>' ); ?>
		
		<?php if( !is_single() ) comments_popup_link( __( '0', 'premitheme' ), __( '1', 'premitheme' ), '%', 'comments-link', __( '--', 'premitheme' ) ); ?>
		
		<div class="clear"></div>
	</div>
</article>
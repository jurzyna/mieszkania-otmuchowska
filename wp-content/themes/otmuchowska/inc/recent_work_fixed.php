<div class=" jcarousel-skin-work"><div class="jcarousel-container jcarousel-container-horizontal" style="position: relative; display: block; "><div class="jcarousel-clip jcarousel-clip-horizontal" style="position: relative; ">

<ul class="recent-work jcarousel-list jcarousel-list-horizontal" style="overflow: hidden; position: relative; top: 0px; margin: 0px; padding: 0px; left: 0px; width: 2287px; ">
    <li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-2 jcarousel-item-2-horizontal" style="float: left; list-style-type: none; list-style-position: initial; list-style-image: initial; " jcarouselindex="2">
    	<a class="folio-overlay" href="http://otmuchowska.nysa.pl/promocje/mieszkanie-21d/" >
    		<img width="240" height="240" src="http://otmuchowska.nysa.pl/wp-content/uploads/2013/05/21D_sprzedane.jpg" class="attachment-folio-3-col wp-post-image" alt="" title="" style="opacity: 1; ">		<h2 class="front_show">Mieszkanie 21D</h2>	
    		<div class="folio-title" style="opacity: 0; ">
    			<h2>Mieszkanie 21D</h2>
    		</div>
    		
    		<span class="more-hover" style="right: -42px; bottom: -42px; opacity: 0; "></span>
    	</a>
    </li>
    
    <li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-2 jcarousel-item-2-horizontal" style="float: left; list-style-type: none; list-style-position: initial; list-style-image: initial; " jcarouselindex="2">
    		<a class="folio-overlay" href="http://otmuchowska.nysa.pl/promocje/mieszkanie-2c/">
    			<img width="240" height="240" src="http://otmuchowska.nysa.pl/wp-content/uploads/2013/05/2c.jpg" class="attachment-folio-3-col wp-post-image" alt="" title="" style="opacity: 1; ">		<h2 class="front_show">Mieszkanie 2C</h2>	
    			<div class="folio-title" style="opacity: 0; ">
    				<h2>Mieszkanie 2C</h2>
    			</div>
    			
    			<span class="more-hover" style="right: -42px; bottom: -42px; opacity: 0; "></span>
    		</a>
    	</li>
    	
    		
    	<li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-3 jcarousel-item-3-horizontal" style="float: left; list-style-type: none; list-style-position: initial; list-style-image: initial; " jcarouselindex="3">
    		<a class="folio-overlay" href="http://otmuchowska.nysa.pl/promocje/mieszkanie-3c/">
    			<img width="240" height="240" src="http://otmuchowska.nysa.pl/wp-content/uploads/2013/05/3c.jpg" class="attachment-folio-3-col wp-post-image" alt="" title="" style="opacity: 1; ">	
    			<h2 class="front_show">Mieszkanie 3C</h2>		
    			<div class="folio-title" style="opacity: 0; ">
    				<h2>Mieszkanie 3C</h2>
    			</div>
    			
    			<span class="more-hover" style="right: -42px; bottom: -42px; opacity: 0; "></span>
    		</a>
    	</li>
    
    	<li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal" style="float: left; list-style-type: none; list-style-position: initial; list-style-image: initial; " jcarouselindex="1">
    		<a class="folio-overlay" href="http://otmuchowska.nysa.pl/promocje/mieszkanie-nr-12c/">
    			<img width="240" height="240" src="http://otmuchowska.nysa.pl/wp-content/uploads/2013/05/12c.jpg" class="attachment-folio-3-col wp-post-image" alt="" title="" style="opacity: 1; ">
    			<h2 class="front_show">Mieszkanie 12C</h2>			
    			<div class="folio-title" style="opacity: 0; ">
    				<h2>Mieszkanie 12C</h2>
    			</div>
    			
    			<span class="more-hover" style="right: -42px; bottom: -42px; opacity: 0; "></span>
    		</a>
    	</li>
    
    
    <li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-2 jcarousel-item-2-horizontal" style="float: left; list-style-type: none; list-style-position: initial; list-style-image: initial; " jcarouselindex="2">
		<a class="folio-overlay" href="http://otmuchowska.nysa.pl/promocje/mieszkanie-13c/" >
			<img width="240" height="240" src="http://otmuchowska.nysa.pl/wp-content/uploads/2013/05/13c.jpg" class="attachment-folio-3-col wp-post-image" alt="" title="" style="opacity: 1; ">		<h2 class="front_show">Mieszkanie 13C</h2>	
			<div class="folio-title" style="opacity: 0; ">
				<h2>Mieszkanie 13C</h2>
			</div>
			
			<span class="more-hover" style="right: -42px; bottom: -42px; opacity: 0; "></span>
		</a>
	</li>
	
		
	<li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-3 jcarousel-item-3-horizontal" style="float: left; list-style-type: none; list-style-position: initial; list-style-image: initial; " jcarouselindex="3">
		<a class="folio-overlay" href="http://otmuchowska.nysa.pl/promocje/mieszkanie-2d/">
			<img width="240" height="240" src="http://otmuchowska.nysa.pl/wp-content/uploads/2013/05/2d.jpg" class="attachment-folio-3-col wp-post-image" alt="" title="" style="opacity: 1; ">	
			<h2 class="front_show">Mieszkanie 2D</h2>		
			<div class="folio-title" style="opacity: 0; ">
				<h2>Mieszkanie 2D</h2>
			</div>
			
			<span class="more-hover" style="right: -42px; bottom: -42px; opacity: 0; "></span>
		</a>
	</li>

	<li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal" style="float: left; list-style-type: none; list-style-position: initial; list-style-image: initial; " jcarouselindex="1">
		<a class="folio-overlay" href="http://otmuchowska.nysa.pl/promocje/mieszkanie-4d/">
			<img width="240" height="240" src="http://otmuchowska.nysa.pl/wp-content/uploads/2013/05/4d.jpg" class="attachment-folio-3-col wp-post-image" alt="" title="" style="opacity: 1; ">
			<h2 class="front_show">Mieszkanie 4D</h2>			
			<div class="folio-title" style="opacity: 0; ">
				<h2>Mieszkanie 4D</h2>
			</div>
			
			<span class="more-hover" style="right: -42px; bottom: -42px; opacity: 0; "></span>
		</a>
	</li>

    <li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-2 jcarousel-item-2-horizontal" style="float: left; list-style-type: none; list-style-position: initial; list-style-image: initial; " jcarouselindex="2">
		<a class="folio-overlay" href="http://otmuchowska.nysa.pl/promocje/mieszkanie-8d/" >
			<img width="240" height="240" src="http://otmuchowska.nysa.pl/wp-content/uploads/2013/05/8d.jpg" class="attachment-folio-3-col wp-post-image" alt="" title="" style="opacity: 1; ">		<h2 class="front_show">Mieszkanie 8D</h2>	
			<div class="folio-title" style="opacity: 0; ">
				<h2>Mieszkanie 8D</h2>
			</div>
			
			<span class="more-hover" style="right: -42px; bottom: -42px; opacity: 0; "></span>
		</a>
	</li>
	
		
	<li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-3 jcarousel-item-3-horizontal" style="float: left; list-style-type: none; list-style-position: initial; list-style-image: initial; " jcarouselindex="3">
		<a class="folio-overlay" href="http://otmuchowska.nysa.pl/promocje/mieszkanie-14d/">
			<img width="240" height="240" src="http://otmuchowska.nysa.pl/wp-content/uploads/2013/05/14d.jpg" class="attachment-folio-3-col wp-post-image" alt="" title="" style="opacity: 1; ">	
			<h2 class="front_show">Mieszkanie 14D</h2>		
			<div class="folio-title" style="opacity: 0; ">
				<h2>Mieszkanie 14D</h2>
			</div>
			
			<span class="more-hover" style="right: -42px; bottom: -42px; opacity: 0; "></span>
		</a>
	</li>

	<li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal" style="float: left; list-style-type: none; list-style-position: initial; list-style-image: initial; " jcarouselindex="1">
		<a class="folio-overlay" href="http://otmuchowska.nysa.pl/promocje/mieszkanie-17d/">
			<img width="240" height="240" src="http://otmuchowska.nysa.pl/wp-content/uploads/2013/05/17d.jpg" class="attachment-folio-3-col wp-post-image" alt="" title="" style="opacity: 1; ">
			<h2 class="front_show">Mieszkanie 17D</h2>			
			<div class="folio-title" style="opacity: 0; ">
				<h2>Mieszkanie 17D</h2>
			</div>
			
			<span class="more-hover" style="right: -42px; bottom: -42px; opacity: 0; "></span>
		</a>
	</li>
	

</ul>
		
		
</div><div class="jcarousel-prev jcarousel-prev-horizontal jcarousel-prev-disabled jcarousel-prev-disabled-horizontal" style="display: block; " disabled="disabled"></div><div class="jcarousel-next jcarousel-next-horizontal" style="display: block; "></div></div></div>


<div class="home-block-title">
	<span></span>
	<h2>Mieszkania Objęte Promocją</h2>
</div>
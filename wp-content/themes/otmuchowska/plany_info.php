<?php
/*
Template Name:Plany Info
*/

$soldText2="<h3 class=\"sold\">Mieszkanie Zostało Sprzedane<h5>";
$soldText="<div class=\"box errorBox\"><p>Mieszkanie Zostało Sprzedane</p>
</div>";
?>
	
	<section id="content-wrap">
		<div id="main">
			
			<?php the_post(); ?>
			
			<article id="post-<?php the_ID();?>" <?php post_class('entry-wrap');?>>
				<?php if ( has_post_thumbnail()): ?>
				<div class="entry-thumb">
					<?php the_post_thumbnail('fullwidth-page-image'); ?>
				</div>
				<?php endif; ?>
				
				<h1 class="entry-title"><?php the_title(); ?></h1>
				
				<div class="entry-content">
					
						<!--       LEVEL 0       -->
									
									<div id="informacja" class="info_l0_1C">
										<?php if(get_option_tree('1c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('1c', '', false, true, 0 ) == "Wolne" || get_option_tree('1c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 2C - <span class="<?php echo get_option_tree('1c'); ?>"><?php echo get_option_tree('2c'); ?></span></h5><?php } ?>
									</div>
									
									<div id="informacja" class="info_l0_2C">
										<?php if(get_option_tree('2c', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('2c', '', false, true, 0 ) == "Wolne" || get_option_tree('2c', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 2C - <span class="<?php echo get_option_tree('2c'); ?>"><?php echo get_option_tree('2c'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">112,12 m2</li>
											
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">18,28 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">8,54 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">18,00 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">5,32 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,56 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">28,45 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">6,87 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">7,86 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Pomieszczenie gospodarcze</li>
											<li class="infoLista2">3,24 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,95 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">8,09 m2 </li>
										</ul>
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 3C" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/2c.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/2c.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									
									<div id="informacja" class="info_l0_3C">
										<?php if(get_option_tree('3c', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('3c', '', false, true, 0 ) == "Wolne" || get_option_tree('3c', '', false, true, 0 ) == "Rezerwacja") { ?>
										
										<h5>Miesazkanie 3c - <span class="<?php echo get_option_tree('3c'); ?>"><?php echo get_option_tree('3c'); ?></span></h5>
																				
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">53,35 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój </li>
											<li class="infoLista2">9,84 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">20,49 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">8,19 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia </li>
											<li class="infoLista2">10,12 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,71 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,95 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">6,37 m2</li>
										</ul>
										
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 3C" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/3c.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/3c.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																				
										<?php } ?>
									
									</div>
									
									
									<div id="informacja" class="info_l0_4C">
										<?php if(get_option_tree('4c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('4c', '', false, true, 0 ) == "Wolne" || get_option_tree('4c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 4c - <span class="<?php echo get_option_tree('4c'); ?>"><?php echo get_option_tree('4c'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l0_5C">
										<?php if(get_option_tree('5c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('5c', '', false, true, 0 ) == "Wolne" || get_option_tree('5c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 5c - <span class="<?php echo get_option_tree('5c'); ?>"><?php echo get_option_tree('5c'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l0_1D">
										<?php if(get_option_tree('1d', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('1d', '', false, true, 0 ) == "Wolne" || get_option_tree('1d', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 1d (Wariant 1) - <span class="<?php echo get_option_tree('1d'); ?>"><?php echo get_option_tree('1d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">50,97 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,42 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">5,46 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">20,00 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">17, 74 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,35 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">6,39 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 1d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/1d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/1d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<h5>Miesazkanie 1d (Wariant 2) - <span class="<?php echo get_option_tree('1d'); ?>"><?php echo get_option_tree('1d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">50,97 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">9,73 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny  z aneksem kuchennym</li>
											<li class="infoLista2">21,23 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,06 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,41 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">6,39 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 1d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/1_1d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/1_1d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l0_2D">
										<?php if(get_option_tree('2d', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('2d', '', false, true, 0 ) == "Wolne" || get_option_tree('2d', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 2d - <span class="<?php echo get_option_tree('2d'); ?>"><?php echo get_option_tree('2d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">49,68 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">10,52 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">8,52 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">7,92  m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój</li>
											<li class="infoLista2">22,89 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">5,99 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 2d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/2D.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/2D.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l0_3D">
										<?php if(get_option_tree('3d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('3d', '', false, true, 0 ) == "Wolne" || get_option_tree('3d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 3d - <span class="<?php echo get_option_tree('3d'); ?>"><?php echo get_option_tree('3d'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l0_4D">
										<?php if(get_option_tree('4d', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('4d', '', false, true, 0 ) == "Wolne" || get_option_tree('4d', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 4d - <span class="<?php echo get_option_tree('4d'); ?>"><?php echo get_option_tree('4d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,04 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">9,11 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,06 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">22,68 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">6,82 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,37 m2 </li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">4,37 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 3d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/4d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/4d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<h5>Miesazkanie 4d - <span class="<?php echo get_option_tree('4d'); ?>"><?php echo get_option_tree('4d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,04 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">12,25 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,02 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,25 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny z aneksem kuchennym</li>
											<li class="infoLista2">19,35 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,37 m2 </li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">4,37 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 3d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/4_4d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/4_4d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l0_5D">
										Hello
									</div>
									
									
									<div id="informacja" class="info_l0_6D">
										<?php if(get_option_tree('6d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('6d', '', false, true, 0 ) == "Wolne" || get_option_tree('6d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 6d - <span class="<?php echo get_option_tree('6d'); ?>"><?php echo get_option_tree('6d'); ?></span></h5><?php } ?>
									</div>
									
									
									<!--       LEVEL !       -->
									
									<div id="informacja" class="info_l1_6C">
										<?php if(get_option_tree('6c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('6c', '', false, true, 0 ) == "Wolne" || get_option_tree('6c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 6c - <span class="<?php echo get_option_tree('6c'); ?>"><?php echo get_option_tree('6c'); ?></span></h5><?php } ?>
									</div>
									
									<div id="informacja" class="info_l1_7C">
										<?php if(get_option_tree('7c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('7c', '', false, true, 0 ) == "Wolne" || get_option_tree('7c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 7c - <span class="<?php echo get_option_tree('7c'); ?>"><?php echo get_option_tree('7c'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l1_8C">
										<?php if(get_option_tree('8c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('8c', '', false, true, 0 ) == "Wolne" || get_option_tree('8c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 8c - <span class="<?php echo get_option_tree('8c'); ?>"><?php echo get_option_tree('8c'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l1_9C">
										<?php if(get_option_tree('9c', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('9c', '', false, true, 0 ) == "Wolne" || get_option_tree('9c', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 9c - <span class="<?php echo get_option_tree('9c'); ?>"><?php echo get_option_tree('9c'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">77,19 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój  </li>
											<li class="infoLista2">16,81 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,02 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">22,81 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,91 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">12,70 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,95 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">7,39 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 9C" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/9c.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/9c.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l1_10C">
										<?php if(get_option_tree('10c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('10c', '', false, true, 0 ) == "Wolne" || get_option_tree('10c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 8c - <span class="<?php echo get_option_tree('1c'); ?>"><?php echo get_option_tree('10c'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l1_7D">
										<?php if(get_option_tree('7d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('7d', '', false, true, 0 ) == "Wolne" || get_option_tree('7d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 7d - <span class="<?php echo get_option_tree('7d'); ?>"><?php echo get_option_tree('7d'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l1_8D">
										<?php if(get_option_tree('8d', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('8d', '', false, true, 0 ) == "Wolne" || get_option_tree('8d', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 8d (Wariant 1) - <span class="<?php echo get_option_tree('8d'); ?>"><?php echo get_option_tree('8d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">51,89 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,44 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,09 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">13,68 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">22,66 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,02 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">8,09 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 8d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/8d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/8d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<h5>Miesazkanie 8d (Wariant 2) - <span class="<?php echo get_option_tree('8d'); ?>"><?php echo get_option_tree('8d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">4,44 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,44 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,09 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny z aneksem kuchennym</li>
											<li class="infoLista2">28,49 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,02 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,87 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">8,09 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 8d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/8_8d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/8_8d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l1_9D">
										<?php if(get_option_tree('9d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('9d', '', false, true, 0 ) == "Wolne" || get_option_tree('9d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 9d - <span class="<?php echo get_option_tree('9d'); ?>"><?php echo get_option_tree('9d'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l1_10D">
										<?php if(get_option_tree('10d', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('10d', '', false, true, 0 ) == "Wolne" || get_option_tree('10d', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 10d - <span class="<?php echo get_option_tree('10d'); ?>"><?php echo get_option_tree('10d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,37 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">9,27 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,32 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">23,04 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">5,92 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,82 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">11,72 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 10d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/10d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/10d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<h5>Miesazkanie 10d - <span class="<?php echo get_option_tree('10d'); ?>"><?php echo get_option_tree('10d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,37 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">12,70 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,06m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,32 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny z aneksem kuchennym</li>
											<li class="infoLista2">18,47 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,82 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">11,72 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 10d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/10_10d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/10_10d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l1_11D">
										<?php if(get_option_tree('11d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('11d', '', false, true, 0 ) == "Wolne" || get_option_tree('11d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 11d - <span class="<?php echo get_option_tree('11d'); ?>"><?php echo get_option_tree('11d'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l1_12D">
										<?php if(get_option_tree('12d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('12d', '', false, true, 0 ) == "Wolne" || get_option_tree('12d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 12d - <span class="<?php echo get_option_tree('12d'); ?>"><?php echo get_option_tree('12d'); ?></span></h5><?php } ?>
									</div>
									
									
									<!--       LEVEL 2       -->
									
									<div id="informacja" class="info_l2_11C">
										<?php if(get_option_tree('11c', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('11c', '', false, true, 0 ) == "Wolne" || get_option_tree('11c', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 11c - <span class="<?php echo get_option_tree('11c'); ?>"><?php echo get_option_tree('11c'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">66,67 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">13,08 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">4,68 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">20,19 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">8,40 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">13,53 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">6,78 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,95 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">8,56 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 11C" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/11c.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/11c.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l2_12C">
										<?php if(get_option_tree('12c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('12c', '', false, true, 0 ) == "Wolne" || get_option_tree('12c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 12c - <span class="<?php echo get_option_tree('12c'); ?>"><?php echo get_option_tree('12c'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">112,96 m2</li>
										</ul>
									
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">17,57 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">8,88 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">6,57 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">32,54 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">7,83 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pomieszczenie gospodarcze</li>
											<li class="infoLista2">3,89 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">8,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,58 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">17,16 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">15,75 m2</li>
										</ul>	
										
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 21C" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/08/12c.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/08/12c.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l2_13C">
										<?php if(get_option_tree('13c', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('13c', '', false, true, 0 ) == "Wolne" || get_option_tree('13c', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 13c - <span class="<?php echo get_option_tree('13c'); ?>"><?php echo get_option_tree('13c'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">52,13 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">9,04 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">20,92 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">8,31  m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,99 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2"></li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">6,46 m2</li>
										</ul>	

										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 13C" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/13c.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/13c.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l2_14C">
										<?php if(get_option_tree('14c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('14c', '', false, true, 0 ) == "Wolne" || get_option_tree('14c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 14c - <span class="<?php echo get_option_tree('14c'); ?>"><?php echo get_option_tree('14c'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l2_15C">
										<?php if(get_option_tree('15c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('15c', '', false, true, 0 ) == "Wolne" || get_option_tree('15c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 15c - <span class="<?php echo get_option_tree('15c'); ?>"><?php echo get_option_tree('15c'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l2_13D">
										<?php if(get_option_tree('13d', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('13d', '', false, true, 0 ) == "Wolne" || get_option_tree('13d', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 13d (Wariant 1)- <span class="<?php echo get_option_tree('13d'); ?>"><?php echo get_option_tree('13d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">52,84 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,25 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">5,68 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">20,47 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">19,06 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,38 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">9,40 m2</li>
										</ul>	
										
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 13d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/13d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/13d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<h5>Miesazkanie 13d (Wariant 2)- <span class="<?php echo get_option_tree('13d'); ?>"><?php echo get_option_tree('13d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">9,61 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">5,68 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny  z aneksem kuchennym</li>
											<li class="infoLista2">23,17 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,42 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">6,99 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,65 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">9,40 m2</li>
										</ul>	
										
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 13d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/13_13d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/13_13d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l2_14D">
										<?php if(get_option_tree('14d', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('14d', '', false, true, 0 ) == "Wolne" || get_option_tree('14d', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 14d (Wariant 1)- <span class="<?php echo get_option_tree('14d'); ?>"><?php echo get_option_tree('14d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">50,76 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,08 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,72 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">13,39 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">22,27 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,30 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">3,07 m2</li>
										</ul>	
	
										<a class="alignnone fancybox sideImage" title="Mieszkanie 14d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/14d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/14d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<h5>Miesazkanie 14d (Wariant 2)- <span class="<?php echo get_option_tree('14d'); ?>"><?php echo get_option_tree('14d'); ?></span></h5>
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">50,76 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,07 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,16 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny z aneksem kuchennym</li>
											<li class="infoLista2">27,83 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,83 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">6,87 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">3,07 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 14d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/14_14d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/14_14d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l2_15D">
										<?php if(get_option_tree('15d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('15d', '', false, true, 0 ) == "Wolne" || get_option_tree('15d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 15d - <span class="<?php echo get_option_tree('15d'); ?>"><?php echo get_option_tree('15d'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l2_16D">
										<?php if(get_option_tree('16d', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('16d', '', false, true, 0 ) == "Wolne" || get_option_tree('16d', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 16d (Wariant 1) - <span class="<?php echo get_option_tree('16d'); ?>"><?php echo get_option_tree('16d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,30 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">7,83 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,32 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">23,10 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">5,69 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,92 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">6,64 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 16d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/16d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/16d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<h5>Miesazkanie 16d (Wariant 2) - <span class="<?php echo get_option_tree('16d'); ?>"><?php echo get_option_tree('16d'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,30 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">12,56 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,08m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,32 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny z aneksem kuchennym</li>
											<li class="infoLista2">18,36 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,98 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">6,64 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 16d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/16_16d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/16_16d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l2_17D">
																			<?php if(get_option_tree('17d', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																																							
																												
																			<?php if(get_option_tree('17d', '', false, true, 0 ) == "Wolne" || get_option_tree('17d', '', false, true, 0 ) == "Rezerwacja") { ?>
																													
																												
																			<h5>Miesazkanie 17d - <span class="<?php echo get_option_tree('17d'); ?>"><?php echo get_option_tree('17d'); ?></span></h5>
																																							
																												
																			<ul class="infoLista first">
																				<li class="infoLista1">Powierzchnia</li>
																				<li class="infoLista2">53,76 m2</li>
																			</ul>
																			
																			
																			<ul class="infoLista">
																				<li class="infoLista1">Przedpokój</li>
																				<li class="infoLista2">9,08 m2</li>
																			</ul>	
																			
																			<ul class="infoLista">
																				<li class="infoLista1">Sypialnia</li>
																				<li class="infoLista2">11,72 m2</li>
																			</ul>	
																			
																			<ul class="infoLista">
																				<li class="infoLista1">Pokój dzienny</li>
																				<li class="infoLista2">23,39 m2</li>
																			</ul>	
																			
																			<ul class="infoLista">
																				<li class="infoLista1">Kuchnia</li>
																				<li class="infoLista2"> 5,95 m2</li>
																			</ul>	
																			
																			<ul class="infoLista">
																				<li class="infoLista1">Łazienka</li>
																				<li class="infoLista2">3,62 m2</li>
																			</ul>	
																			
																			<ul class="infoLista">
																				<li class="infoLista1">Piwnica</li>
																				<li class="infoLista2">9,97 m2</li>
																			</ul>	
									
																			
																			<a class="alignnone fancybox sideImage" title="Mieszkanie 17d" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/17d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/17d.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																													
																			<?php } ?>
																		</div>
									
									
									<div id="informacja" class="info_l2_18D">
										<?php if(get_option_tree('18d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('18d', '', false, true, 0 ) == "Wolne" || get_option_tree('18d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 18d - <span class="<?php echo get_option_tree('18d'); ?>"><?php echo get_option_tree('18d'); ?></span></h5><?php } ?>
									</div>
									
									
									<!--       LEVEL 3       -->
									
									<div id="informacja" class="info_l3_16C">
										<?php if(get_option_tree('16c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('16c', '', false, true, 0 ) == "Wolne" || get_option_tree('16c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 16c - <span class="<?php echo get_option_tree('16c'); ?>"><?php echo get_option_tree('16c'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l3_17C">
										<?php if(get_option_tree('17c', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('17c', '', false, true, 0 ) == "Wolne" || get_option_tree('17c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 17c - <span class="<?php echo get_option_tree('17c'); ?>"><?php echo get_option_tree('17c'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l3_18C">
										<?php if(get_option_tree('18c', '', false, true, 0 ) == "Sprzedane" ) { echo "$soldText"; } ?>
																														
																			
										<?php if(get_option_tree('18c', '', false, true, 0 ) == "Wolne" || get_option_tree('18c', '', false, true, 0 ) == "Rezerwacja") { ?>
																				
																			
										<h5>Miesazkanie 18c - <span class="<?php echo get_option_tree('18c'); ?>"><?php echo get_option_tree('18c'); ?></span></h5>
																														
																			
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,03 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">7,40 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">9,09  m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">21,63 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,69 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">5,22 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">5,60 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">6,63 m2</li>
										</ul>	
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 18C" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/18c.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/07/18c.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
																														
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l3_19C">
											<?php if(get_option_tree('19c', '', false, true, 0 ) == "Sprzedane" ) {
																					echo "$soldText"; } ?>
																					
											<?php if(get_option_tree('19c', '', false, true, 0 ) == "Wolne" || get_option_tree('19c', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 19c - <span class="<?php echo get_option_tree('19c'); ?>"><?php echo get_option_tree('19c'); ?></span></h5><?php } ?>
										
									</div>
									
									
									
									<div id="informacja" class="info_l3_19D">
									
										<?php if(get_option_tree('19d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('19d', '', false, true, 0 ) == "Wolne" || get_option_tree('19d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 19d - <span class="<?php echo get_option_tree('19d'); ?>"><?php echo get_option_tree('19d'); ?></span></h5><?php } ?>
										
									</div>
									
									
									<div id="informacja" class="info_l3_20D">
										<?php if(get_option_tree('20d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('20d', '', false, true, 0 ) == "Wolne" || get_option_tree('20d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 20d - <span class="<?php echo get_option_tree('20d'); ?>"><?php echo get_option_tree('20d'); ?></span></h5><?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l3_21D">
										<?php if(get_option_tree('21d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('21d', '', false, true, 0 ) == "Wolne" || get_option_tree('21d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 21d - <span class="<?php echo get_option_tree('21d'); ?>"><?php echo get_option_tree('21d'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">82,53 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">I kondygnacja:</li>
											<li class="infoLista2">82,53 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">16,96 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">16,53 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Jadalnia/ Pokój dzienny</li>
											<li class="infoLista2">23,64  m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">7,47 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">15,24 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Garderoba</li>
											<li class="infoLista2">2,69 m2</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">5,60 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">5,60 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">II kondygnacja:</li>
											<li class="infoLista2">20,00 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Pokój</li>
										</ul>
										<ul class="infoLista">
											<li class="infoLista1">Pokój</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Piwnica</li>
											<li class="infoLista2">16,38 m2</li>
										</ul>
												
										<a class="alignnone fancybox sideImage" title="Mieszkanie 21D" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/08/21d.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/08/12c.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										<a class="alignnone fancybox sideImage" title="Mieszkanie 21D - II Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2012/08/21d_2.jpg" rel="prettyPhoto[slides]" style="width: 275px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2012/08/21d_2.jpg&amp;h=191&amp;w=275&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										<?php } ?>
									</div>
									
									
									<div id="informacja" class="info_l3_22D">
										<?php if(get_option_tree('22d', '', false, true, 0 ) == "Sprzedane" ) {
																				echo "$soldText"; } ?>
																				
										<?php if(get_option_tree('22d', '', false, true, 0 ) == "Wolne" || get_option_tree('22d', '', false, true, 0 ) == "Rezerwacja") { ?><h5>Miesazkanie 22d - <span class="<?php echo get_option_tree('22d'); ?>"><?php echo get_option_tree('22d'); ?></span></h5><?php } ?>
									</div>
									
									
									<!--       KLATKA A       -->
									
									
									
									<div id="informacja" class="info_l0_1A">
										<?php if(get_option_tree('1a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('1a', '', false, true, 0 ) == "Wolne" || get_option_tree('1a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 1A - <span class="<?php echo get_option_tree('1a'); ?>"><?php echo get_option_tree('1a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">33,55  m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,88  m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">6,05  m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">18,74 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,88 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94  m2</li>
										</ul>	
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 1A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_1_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_1_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l0_2A">
										<?php if(get_option_tree('2a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('2a', '', false, true, 0 ) == "Wolne" || get_option_tree('2a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 2A - <span class="<?php echo get_option_tree('2a'); ?>"><?php echo get_option_tree('2a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">53,74  m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">11,69  m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">5,45 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">19,37 m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Pokój Sypialniany</li>
											<li class="infoLista2">12,74 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,49 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">2,94  m2</li>
										</ul>	
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 2A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_2_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_2_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>

									<div id="informacja" class="info_l0_2Av2">
										<?php if(get_option_tree('2av2', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('2av2', '', false, true, 0 ) == "Wolne" || get_option_tree('2a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 3A - <span class="<?php echo get_option_tree('2av2'); ?>"><?php echo get_option_tree('2a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">41,78 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">3,79  m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,20  m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">5,48 m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">28,31 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3  m2</li>
										</ul>	
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 3A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_3_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_3_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l0_3A">
										<?php if(get_option_tree('3a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('3a', '', false, true, 0 ) == "Wolne" || get_option_tree('3a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 4A - <span class="<?php echo get_option_tree('3a'); ?>"><?php echo get_option_tree('3a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">68,52 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">12,47 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">10,08 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">19,29 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,73 m2</li>
										</ul>	

										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,11 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,84 m2</li>
										</ul>
											
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 4A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_4_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_4_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l0_4A">
										<?php if(get_option_tree('4a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('4a', '', false, true, 0 ) == "Wolne" || get_option_tree('4a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 5A (Wariant 1) - <span class="<?php echo get_option_tree('4a'); ?>"><?php echo get_option_tree('4a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">33,14 m2</li>
										</ul>
									
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">3,63 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">5,21 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">20,50 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,80 m2</li>
										</ul>	

											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3  m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 5A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_5_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_5_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l0_5A">
										<?php if(get_option_tree('5a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('5a', '', false, true, 0 ) == "Wolne" || get_option_tree('5a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 6A - <span class="<?php echo get_option_tree('5a'); ?>"><?php echo get_option_tree('5a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">76,98 m2</li>
										</ul>

										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">7,76 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">7,36 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">33,37 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">6,79 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,98 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,72 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 6A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_6_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_6_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l0_6A">
										<?php if(get_option_tree('6a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('6a', '', false, true, 0 ) == "Wolne" || get_option_tree('6a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 7A - <span class="<?php echo get_option_tree('6a'); ?>"><?php echo get_option_tree('6a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">86,03 m2</li>
										</ul>

										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">9,28 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">8,43 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">20,54 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pomieszczenie gospodarcze</li>
											<li class="infoLista2">2,76 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">5,65 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,53 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">15,06 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,89 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 7A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_7_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_7_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l1_7A">
										<?php if(get_option_tree('7a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('7a', '', false, true, 0 ) == "Wolne" || get_option_tree('7a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 8A - <span class="<?php echo get_option_tree('7a'); ?>"><?php echo get_option_tree('7a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">33,93 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">3,38 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">5,36 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">21,42 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,77 m2</li>
										</ul>	
																					
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 8A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_8_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_8_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l1_8A">
										<?php if(get_option_tree('8a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('8a', '', false, true, 0 ) == "Wolne" || get_option_tree('8a', '', false, true, 0 ) == "Rezerwacja") { ?>

										<h5>Miesazkanie 9A - <span class="<?php echo get_option_tree('8a'); ?>"><?php echo get_option_tree('8a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,31 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">11,98 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">5,61 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">19,38 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">13,08 m2</li>
										</ul>	

										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,26  m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3  m2</li>
										</ul>	
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 9A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_9_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_9_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<?php } ?>
										
									</div>
									<div id="informacja" class="info_l1_8Av2">
										<?php if(get_option_tree('8av2', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('8av2', '', false, true, 0 ) == "Wolne" || get_option_tree('8av2', '', false, true, 0 ) == "Rezerwacja") { ?>
										
										<h5>Miesazkanie 10A - <span class="<?php echo get_option_tree('8av2'); ?>"><?php echo get_option_tree('8av2'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">42,44 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">5,12 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,28 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">6,66 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">26,38 m2</li>
										</ul>	

										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3  m2</li>
										</ul>	
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 10A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_10_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_10_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<?php } ?>
										
									</div>
									
										

									<div id="informacja" class="info_l1_9A">
										<?php if(get_option_tree('9a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('9a', '', false, true, 0 ) == "Wolne" || get_option_tree('9a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 11A - <span class="<?php echo get_option_tree('9a'); ?>"><?php echo get_option_tree('9a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">69,7 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">11,12 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">10,39 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">21,36 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,22 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,13 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,57  m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3  m2</li>
										</ul>	
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 11A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_11_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_11_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l1_10A">
										<?php if(get_option_tree('10a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('10a', '', false, true, 0 ) == "Wolne" || get_option_tree('10a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 12A <span class="<?php echo get_option_tree('10a'); ?>"><?php echo get_option_tree('10a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">34,16 m2</li>
										</ul>
									
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">5,76 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">5,89 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">18,57 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,94 m2</li>
										</ul>	

											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3  m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 12A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_12_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_12_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l1_11A">
										<?php if(get_option_tree('11a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('11a', '', false, true, 0 ) == "Wolne" || get_option_tree('11a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 13A - <span class="<?php echo get_option_tree('11a'); ?>"><?php echo get_option_tree('11a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">76,36 m2</li>
										</ul>

										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">8,21 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">8,44 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">34,09 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">6,53 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,25 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">1,84 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 13A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_13_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_13_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l1_12A">
										<?php if(get_option_tree('12a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('12a', '', false, true, 0 ) == "Wolne" || get_option_tree('12a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 14A - <span class="<?php echo get_option_tree('12a'); ?>"><?php echo get_option_tree('12a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">88,73 m2</li>
										</ul>

										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">6,10 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">7,84 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">24,24 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,72 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">20,16 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">8,10 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,24 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">WC</li>
											<li class="infoLista2">1,69 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 14A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_14_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_14_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_13A">
										<?php if(get_option_tree('13a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('13a', '', false, true, 0 ) == "Wolne" || get_option_tree('13a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 15A - <span class="<?php echo get_option_tree('13a'); ?>"><?php echo get_option_tree('13a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">33,71 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">3,22 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">4,98 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">21,65 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,86 m2</li>
										</ul>	
																					
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 15A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-15_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-15_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_14A">
										<?php if(get_option_tree('14a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('14a', '', false, true, 0 ) == "Wolne" || get_option_tree('14a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 16A - <span class="<?php echo get_option_tree('14a'); ?>"><?php echo get_option_tree('14a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,11 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">11,95 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">5,06 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">19,80 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,95 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 16A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-16_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-16_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<?php } ?>
										
									</div>

									<div id="informacja" class="info_l2_14Av2">
										<?php if(get_option_tree('14av2', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('14av2', '', false, true, 0 ) == "Wolne" || get_option_tree('14av2', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 17A - <span class="<?php echo get_option_tree('14av2'); ?>"><?php echo get_option_tree('14av2'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">42,30 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">5,22 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,29 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">8,25 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">24,54 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3  m2</li>
										</ul>	
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 17A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-17_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-17_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_15A">
										<?php if(get_option_tree('15a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('15a', '', false, true, 0 ) == "Wolne" || get_option_tree('15a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 18A - <span class="<?php echo get_option_tree('15a'); ?>"><?php echo get_option_tree('15a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">69,69 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">11,41 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">9,78 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">21,46 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,62 m2</li>
										</ul>	

										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,18 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,24 m2</li>
										</ul>
											
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 18A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-18_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-18_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_16A">
										<?php if(get_option_tree('16a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('16a', '', false, true, 0 ) == "Wolne" || get_option_tree('16a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 19A - <span class="<?php echo get_option_tree('16a'); ?>"><?php echo get_option_tree('16a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">34,14 m2</li>
										</ul>
									
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">3,78 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">5,04 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">21,26 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,06 m2</li>
										</ul>	

											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3  m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 19A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-19_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-19_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_17A">
										<?php if(get_option_tree('17a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('17a', '', false, true, 0 ) == "Wolne" || get_option_tree('17a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 20A - <span class="<?php echo get_option_tree('17a'); ?>"><?php echo get_option_tree('17a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">79,58 m2</li>
										</ul>

										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">8,37 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">9,53 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">34,21 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">6,55 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,15 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">1,77 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 20A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-20_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-20_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_18A">
										<?php if(get_option_tree('18a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('18a', '', false, true, 0 ) == "Wolne" || get_option_tree('18a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 21A - <span class="<?php echo get_option_tree('18a'); ?>"><?php echo get_option_tree('18a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">87,95 m2</li>
										</ul>

										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">6,07 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">6,95 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">25,16 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">WC</li>
											<li class="infoLista2">1,66 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,74 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">19,58 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,92 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,41 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 21A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-21_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-21_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_19A">
										<?php if(get_option_tree('19a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('19a', '', false, true, 0 ) == "Wolne" || get_option_tree('19a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 22A - <span class="<?php echo get_option_tree('19a'); ?>"><?php echo get_option_tree('19a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">62,04 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">9,21 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">8,70 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">16,80 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">5,31 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">2,70 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">8,96 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,36 m2</li>
										</ul>
											
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 22A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_22_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_22_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_20A">
										<?php if(get_option_tree('20a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('20a', '', false, true, 0 ) == "Wolne" || get_option_tree('20a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 23A - <span class="<?php echo get_option_tree('20a'); ?>"><?php echo get_option_tree('20a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">63,42 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">18,20 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">6,98 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">17,54 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">5,25 m2</li>
										</ul>	
										
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Poddasze użytkowe</li>
											<li class="infoLista2">15,45 m2</li>
										</ul>
										
										<a class="alignnone fancybox sideImage" title="Mieszkanie 23A - I Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_23_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_23_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										<a class="alignnone fancybox sideImage" title="Mieszkanie 23A - II Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_23_A_S.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_23_A_S.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_21A">
										<?php if(get_option_tree('21a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('21a', '', false, true, 0 ) == "Wolne" || get_option_tree('21a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 24A - <span class="<?php echo get_option_tree('21a'); ?>"><?php echo get_option_tree('21a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">74,07 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">5,96 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">7,42 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">26,48 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">6,31 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,00 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">II kondygnacja:</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,95 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">12,91 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 24A - I Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_24_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_24_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										<a class="alignnone fancybox sideImage" title="Mieszkanie 24A - II Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_24_A_S.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_24_A_S.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_22A">
										<?php if(get_option_tree('22a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('22a', '', false, true, 0 ) == "Wolne" || get_option_tree('22a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 25A - <span class="<?php echo get_option_tree('22a'); ?>"><?php echo get_option_tree('22a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">44,96 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">I kondygnacja:</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">3,04 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">16,06 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,33 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">II kondygnacja:</li>
										</ul>

										
										<ul class="infoLista">
											<li class="infoLista1">Poddasze użytkowe</li>
											<li class="infoLista2">22,53 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 25A - I Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_25_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_25_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										<a class="alignnone fancybox sideImage" title="Mieszkanie 25A - II Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_25_A_S.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_25_A_S.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_23A">
										<?php if(get_option_tree('23a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('23a', '', false, true, 0 ) == "Wolne" || get_option_tree('23a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 26A - <span class="<?php echo get_option_tree('23a'); ?>"><?php echo get_option_tree('23a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">88,64 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">I kondygnacja:</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">8,64 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">11,58 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">24,38 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">7,17 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój</li>
											<li class="infoLista2">3,92 m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Jadalnia</li>
											<li class="infoLista2">8,49 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">II kondygnacja:</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">7,09 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,54 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,78 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 26A - I Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_26_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_26_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										<a class="alignnone fancybox sideImage" title="Mieszkanie 26A - II Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_26_A_S.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_26_A_S.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_24A">
										<?php if(get_option_tree('24a', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('24a', '', false, true, 0 ) == "Wolne" || get_option_tree('24a', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 27A - <span class="<?php echo get_option_tree('24a'); ?>"><?php echo get_option_tree('24a'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">61,06 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">3,32 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">7,78 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">23,61 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,39 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,95 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">6,44  m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>

																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 27A" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_27_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_27_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
							
								<!--       KLATKA B       -->
									<div id="informacja" class="info_l0_1B">
										<?php if(get_option_tree('1b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('1b', '', false, true, 0 ) == "Wolne" || get_option_tree('1b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 1B - <span class="<?php echo get_option_tree('1b'); ?>"><?php echo get_option_tree('1b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">84,38 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">7,58 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,06 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">5,63 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">25,31 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,72 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,58 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">8,88 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,46 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">15,16 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 1B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_1_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_1_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l0_2B">
										<?php if(get_option_tree('2b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('2b', '', false, true, 0 ) == "Wolne" || get_option_tree('2b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 2B - <span class="<?php echo get_option_tree('2b'); ?>"><?php echo get_option_tree('2b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">74,80 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">1,79 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">7,59 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">7,84 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">24,86 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">5,56 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">17,49 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,67 m2</li>
										</ul>

											
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">1,79 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 2B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_2_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_2_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l0_3B">
										<?php if(get_option_tree('3b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('3b', '', false, true, 0 ) == "Wolne" || get_option_tree('3b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 3B - <span class="<?php echo get_option_tree('3b'); ?>"><?php echo get_option_tree('3b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">52,66 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">9,91 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">16,02 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">2,25 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,47 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,77 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">8,24 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 3B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_3_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_3_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l0_4B">
										<?php if(get_option_tree('4b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('4b', '', false, true, 0 ) == "Wolne" || get_option_tree('4b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 4B - <span class="<?php echo get_option_tree('4b'); ?>"><?php echo get_option_tree('4b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">61,33 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">5,12 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">3,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">16,01 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">6,11 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,14 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">14,22 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 4B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_4_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_4_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l0_5B">
										<?php if(get_option_tree('5b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('5b', '', false, true, 0 ) == "Wolne" || get_option_tree('5b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 5B - <span class="<?php echo get_option_tree('5b'); ?>"><?php echo get_option_tree('5b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">49,79 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">9,12 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">5,94 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">20,79 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,80 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,14 m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 5B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_5_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_5_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l1_6B">
										<?php if(get_option_tree('6b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('6b', '', false, true, 0 ) == "Wolne" || get_option_tree('6b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 6B - <span class="<?php echo get_option_tree('6b'); ?>"><?php echo get_option_tree('6b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">86,96 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">7,58 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,08 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">6,71 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">24,56 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,52 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,69 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,68 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,58 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">16,56 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">6,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 6B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_6_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_6_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l1_7B">
										<?php if(get_option_tree('7b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('7b', '', false, true, 0 ) == "Wolne" || get_option_tree('7b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 7B - <span class="<?php echo get_option_tree('7b'); ?>"><?php echo get_option_tree('7b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">76,31 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">1,77 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">7,55 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">8,04 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">25,11 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Pokój</li>
											<li class="infoLista2">9,38 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">5,62 m2</li>
										</ul>	
										
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,13 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,71 m2</li>
										</ul>

											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 7B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_7_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_7_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l1_8B">
										<?php if(get_option_tree('8b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('8b', '', false, true, 0 ) == "Wolne" || get_option_tree('8b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 8B - <span class="<?php echo get_option_tree('8b'); ?>"><?php echo get_option_tree('8b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,71 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">10,21 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">3,26m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">16,00 </li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,58 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,20 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">8,46 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 8B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_8_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_8_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l1_9B">
										<?php if(get_option_tree('9b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('9b', '', false, true, 0 ) == "Wolne" || get_option_tree('9b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 9B - <span class="<?php echo get_option_tree('9b'); ?>"><?php echo get_option_tree('9b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">62,69 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,67 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">3,85 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">16,04 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">6,57 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,37 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">14,47 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 9B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_9_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_9_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l1_10B">
										<?php if(get_option_tree('10b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('10b', '', false, true, 0 ) == "Wolne" || get_option_tree('10b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 10B - <span class="<?php echo get_option_tree('10b'); ?>"><?php echo get_option_tree('10b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">50,95 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">8,96 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">6,26 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">21,50 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,76 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,47 m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 10B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_10_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_10_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_11B">
										<?php if(get_option_tree('11b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('11b', '', false, true, 0 ) == "Wolne" || get_option_tree('11b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 11B - <span class="<?php echo get_option_tree('11b'); ?>"><?php echo get_option_tree('11b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">87,01 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">7,48 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,53 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">6,36 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">24,69 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">3,99 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,16 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,54 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,00 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">17,27 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 11B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_11_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_11_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_12B">
										<?php if(get_option_tree('12b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('12b', '', false, true, 0 ) == "Wolne" || get_option_tree('12b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 12B - <span class="<?php echo get_option_tree('12b'); ?>"><?php echo get_option_tree('12b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">77,45 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">7,59 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">8,05 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">25,57 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">5,44 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój</li>
											<li class="infoLista2">9,45 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">9,46 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,08 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">1,81 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 12B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_12_A.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_12_A.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_13B">
										<?php if(get_option_tree('13b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('13b', '', false, true, 0 ) == "Wolne" || get_option_tree('13b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 13B - <span class="<?php echo get_option_tree('13b'); ?>"><?php echo get_option_tree('13b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">54,12 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">8,74 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchennym</li>
											<li class="infoLista2">2,97 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">16,98 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,52 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">8,70 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,21 m2</li>
										</ul>

										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 13B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_13_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_13_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_14B">
										<?php if(get_option_tree('14b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('14b', '', false, true, 0 ) == "Wolne" || get_option_tree('14b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 14B - <span class="<?php echo get_option_tree('14b'); ?>"><?php echo get_option_tree('14b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">62,11 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">4,33 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchennym</li>
											<li class="infoLista2">3,36 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój dzienny</li>
											<li class="infoLista2">16,16 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">6,67 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,42 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">14,50 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 14B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_14_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_14_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l2_15B">
										<?php if(get_option_tree('15b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('15b', '', false, true, 0 ) == "Wolne" || get_option_tree('15b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 15B - <span class="<?php echo get_option_tree('15b'); ?>"><?php echo get_option_tree('15b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">51,27 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">8,63 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">6,67 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">21,10 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,00 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,78 m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">3,00 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 15B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_15_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_15_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_16B">
										<?php if(get_option_tree('16b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('16b', '', false, true, 0 ) == "Wolne" || get_option_tree('16b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 16B - <span class="<?php echo get_option_tree('16b'); ?>"><?php echo get_option_tree('16b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">60,79 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">2,96 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">9,93 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">22,62 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,55 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">6,33 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,89 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 16B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_16_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_16_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_17B">
										<?php if(get_option_tree('17b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('17b', '', false, true, 0 ) == "Wolne" || get_option_tree('17b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 17B - <span class="<?php echo get_option_tree('17b'); ?>"><?php echo get_option_tree('17b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">85,67 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">I kondygnacja:</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">13,19 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Kuchnia</li>
											<li class="infoLista2">11,77 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">26,73 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,19 m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">6,23 m2</li>
										</ul>
		
										
										<ul class="infoLista">
											<li class="infoLista1">II kondygnacja</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">10,68 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">12,88 m2</li>
										</ul>	
																	
										<a class="alignnone fancybox sideImage" title="Mieszkanie 17BA - I Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_17_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_17_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										<a class="alignnone fancybox sideImage" title="Mieszkanie 17BA - II Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_17_B_S.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_17_B_S.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_18B">
										<?php if(get_option_tree('18b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('18b', '', false, true, 0 ) == "Wolne" || get_option_tree('18b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 18B - <span class="<?php echo get_option_tree('18b'); ?>"><?php echo get_option_tree('18b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">58,18 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">I kondygnacja:</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">2,82 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">4,63 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">20,48 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,60 m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">7,89 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
														
										
										<ul class="infoLista">
											<li class="infoLista1">II kondygnacja</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Poddasze użytkowe</li>
											<li class="infoLista2">17,76 m2</li>
										</ul>

																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 18B - I Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_18_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_18_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										<a class="alignnone fancybox sideImage" title="Mieszkanie 18B - II Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_18_B_S.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_18_B_S.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_19B">
										<?php if(get_option_tree('19b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('19b', '', false, true, 0 ) == "Wolne" || get_option_tree('19b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 19B - <span class="<?php echo get_option_tree('19b'); ?>"><?php echo get_option_tree('19b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">72,99 m2</li>
										</ul>
										
										
										<ul class="infoLista">
											<li class="infoLista1">I kondygnacja:</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Przedpokój</li>
											<li class="infoLista2">5,49 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">7,73 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">17,27 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">6,21 m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">11,48 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
														
										
										<ul class="infoLista">
											<li class="infoLista1">II kondygnacja</li>
										</ul>
											
										<ul class="infoLista">
											<li class="infoLista1">Hol</li>
											<li class="infoLista2">14,57 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">10,24 m2</li>
										</ul>
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 19B - I Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_19_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_19_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										<a class="alignnone fancybox sideImage" title="Mieszkanie 18B - II Kondygnacja" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_19_B_S.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_19_B_S.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									<div id="informacja" class="info_l3_20B">
										<?php if(get_option_tree('20b', '', false, true, 0 ) == "Sprzedane" ) {
										echo "$soldText"; } ?>
										
										<?php if(get_option_tree('20b', '', false, true, 0 ) == "Wolne" || get_option_tree('20b', '', false, true, 0 ) == "Rezerwacja") { ?>
										<h5>Miesazkanie 20B - <span class="<?php echo get_option_tree('20b'); ?>"><?php echo get_option_tree('20b'); ?></span></h5>
										
										<ul class="infoLista first">
											<li class="infoLista1">Powierzchnia</li>
											<li class="infoLista2">33,64 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Aneks kuchenny</li>
											<li class="infoLista2">6,58 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Pokój Dzienny</li>
											<li class="infoLista2">17,77 m2</li>
										</ul>	
										
										<ul class="infoLista">
											<li class="infoLista1">Łazienka</li>
											<li class="infoLista2">4,15 m2</li>
										</ul>	

										<ul class="infoLista">
											<li class="infoLista1">Sypialnia</li>
											<li class="infoLista2">5,14 m2</li>
										</ul>
										
										<ul class="infoLista">
											<li class="infoLista1">Balkon</li>
											<li class="infoLista2">4,84 m2</li>
										</ul>
														
																				
										<a class="alignnone fancybox sideImage" title="Mieszkanie 20B" href="http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_20_B.jpg" rel="prettyPhoto[slides]" style="width: 270px;"><img class="pt-img frame" src="http://otmuchowska.nysa.pl/wp-content/themes/otmuchowska/functions/timthumb.php?src=http://otmuchowska.nysa.pl/wp-content/uploads/2015/10/plan-M_20_B.jpg&amp;h=191&amp;w=270&amp;zc=1&amp;q=120&amp;a=c" alt=""></a>
										
										
										<?php } ?>
										
									</div>
									
									
									
									
									
								

				</div>
			</article>
			
		</div><!-- #main -->
		
<?php get_footer();?>

				
			
				
			
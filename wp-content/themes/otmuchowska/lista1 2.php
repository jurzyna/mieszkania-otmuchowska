
<?php get_header();?>
	
	<section id="content-wrap">
		<div id="main">
			
			<?php the_post(); ?>
			
			<article id="post-<?php the_ID();?>" <?php post_class('entry-wrap');?>>
				<?php if ( has_post_thumbnail()): ?>
				<div class="entry-thumb">
					<?php the_post_thumbnail('fullwidth-page-image'); ?>
				</div>
				<?php endif; ?>
				
				<h1 class="entry-title"><?php the_title(); ?></h1>
				
				<div class="entry-content">
						<h2 class="listaTytul">Klatka 54D</h2>
					
					<div class="one_fifth_lista content_left listaTitle"><p>NUMER MIESZKANIA</p></div>
					<div class="one_fifth_lista content_left listaTitle"><p>LOKALIZACJA</p></div>
					<div class="one_fifth_lista content_left listaTitle"><p>POWIERZCHNIA</p></div>				
					<div class="one_fifth_lista content_left listaTitle"><p>BALKON</p></div>
					<div class="one_fifth_lista_last content_left listaTitle"><p>STATUS</p></div>
					
				<div class="<?php if(get_option_tree('1d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('1d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('1d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>1D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>65,9d</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('1d')) { ?><p><?php echo get_option_tree('1d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('2d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('2d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('2d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>2C</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>112,9d</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('2d')) { ?><p><?php echo get_option_tree('2d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('3d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('3d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('3d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>3c</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>53,5</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('3d')) { ?><p><?php echo get_option_tree('3d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('4d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('4d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('4d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>4d</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>76</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('4d')) { ?><p><?php echo get_option_tree('4d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('5d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('5d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('5d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>5D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>parter</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>87,3</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('5d')) { ?><p><?php echo get_option_tree('5d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('6d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('6d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('6d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>6D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>66,3</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('6d')) { ?><p><?php echo get_option_tree('6d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('7d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('7d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('7d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>7D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>113,37</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('7d')) { ?><p><?php echo get_option_tree('7d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('8d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('8d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('8d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>8D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>54,9d</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('8d')) { ?><p><?php echo get_option_tree('8d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('9d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('9d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('9d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>9D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>77,5</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('9d')) { ?><p><?php echo get_option_tree('9d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('10d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('10d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('10d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>10C</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>I piętro</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>89d,5</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('10d')) { ?><p><?php echo get_option_tree('10d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('11d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('11d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('11d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>11d</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>67,8</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('4d')) { ?><p><?php echo get_option_tree('4d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('12d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('12d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('12d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>12D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>114,6</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('12d')) { ?><p><?php echo get_option_tree('12d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('13d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('13d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('13d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>13c</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>52,8</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('13d')) { ?><p><?php echo get_option_tree('13d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('14d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('14d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('14d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>14d</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>81,1</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('14d')) { ?><p><?php echo get_option_tree('14d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('15d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('15d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('15d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>15D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>II piętro</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>9d0</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('15d')) { ?><p><?php echo get_option_tree('15d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('16d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('16d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('16d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>16D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>59d,1</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('16d')) { ?><p><?php echo get_option_tree('16d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('17d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('17d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('17d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>17D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>71,6</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('17d')) { ?><p><?php echo get_option_tree('17d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('18d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('18d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('18d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>18D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>73,05</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('18d')) { ?><p><?php echo get_option_tree('18d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('19d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('19d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('19d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>19D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>93</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('19d')) { ?><p><?php echo get_option_tree('19d'); ?></p><?php } ?></div>
				</div>
				
				
				<div class="<?php if(get_option_tree('20d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('20d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('20d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>20D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>93</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('20d')) { ?><p><?php echo get_option_tree('20d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('21d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('21d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('21d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>20D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>93</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('21d')) { ?><p><?php echo get_option_tree('21d'); ?></p><?php } ?></div>
				</div>
				
				<div class="<?php if(get_option_tree('22d', '', false, true, 0 ) == "Wolne" ) {
				echo "listaFree"; } ?><?php if(get_option_tree('22d', '', false, true, 0 ) == "Sprzedane" ) {
				echo "listaSold"; } ?><?php if(get_option_tree('22d', '', false, true, 0 ) == "Rezerwacja" ) {
				echo "listaReserved"; } ?>">					
					<div class="one_fifth_lista content_left listaInfo"><p>22D</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>poddasze</p></div>
					<div class="one_fifth_lista content_left listaInfo"><p>93</p></div>				
					<div class="one_fifth_lista content_left listaInfo"><p>1</p></div>
					<div class="one_fifth_lista_last content_left listaInfoLast"><?php if(get_option_tree('22d')) { ?><p><?php echo get_option_tree('22d'); ?></p><?php } ?></div>
				</div>
				
				
				
					<div class="clear"></div>	
									
				</div>
			</article>
			
		</div><!-- #main -->
		
<?php get_footer();?>